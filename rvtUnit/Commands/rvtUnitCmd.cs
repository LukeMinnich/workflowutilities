﻿
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Helpers;
using rvtUnit.Controls;

namespace rvtUnit.Commands
{

    /// <summary>
    /// Revit external command to do integration.
    /// </summary>
    [Transaction(TransactionMode.Manual)]
    public class rvtUnitCmd : IExternalCommand
    {
        // ======================================================== FIELDS === //

        private Document _activeDoc;

        // ==================================================== PROPERTIES === //

        /// <summary>
        /// Revit external command entry point.
        /// </summary>
        /// <param name="commandData">A ExternalCommandData object which contains reference to Application and View needed by external command.</param>
        /// <param name="message">Error message can be returned by external command.</param>
        /// <param name="elements">Element set could be used for transferring elements between external command and Autodesk Revit.</param>
        /// <returns>
        /// Result tells whether the excution fail, succeed or was canceled by user. If not succeed, Autodesk Revit should undo any changes made by the external command. 
        /// </returns>
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            _activeDoc = commandData.Application.ActiveUIDocument.Document;

            GeneralHelper.ActiveUIDocument = commandData.Application.ActiveUIDocument;

            DoAction();

            // Return Cancelled so that this command is not recorded as modifying Document:
            return Result.Cancelled;
        }

        private void DoAction()
        {
            MainWindowViewModel vm = new MainWindowViewModel();
            MainWindow view = new MainWindow(vm);
            GeneralHelper.SetRevitAsWindowOwner(view);
            view.ShowDialog();
        }
    }  
} 
