
@echo off
if not exist C:\ProgramData\Autodesk\Revit\Addins\2017\Katerra mkdir C:\ProgramData\Autodesk\Revit\Addins\2017\Katerra
xcopy C:\_git\WorkFlowUtilities\KaterraRevitUtilities\bin\Release C:\ProgramData\Autodesk\Revit\Addins\2017\Katerra  /y /c /s 
xcopy C:\_git\WorkFlowUtilities\KaterraUtilities.addin C:\ProgramData\Autodesk\Revit\Addins\2017 /y /c /s 
xcopy C:\_git\WorkFlowUtilities\RevitRemoteControl\bin\Release C:\ProgramData\Autodesk\Revit\Addins\2017\Katerra\RemoteControl  /y /c /s 

xcopy C:\_git\WorkFlowUtilities\RevitRemoteControl\bin\Release C:\_git\WorkFlowUtilities\KaterraRevitUtilities\bin\release\RemoteControl  /y /c /s 
