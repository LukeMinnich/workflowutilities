﻿using Autodesk.Revit.UI;
using log4net;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media.Imaging;

/// <summary>
/// Stuff in this file is mostly used in unit tests and just to make sure that the code is always
/// the same, I am placing these thinhgs in this assembly...
/// </summary>
namespace Helpers
{
	public static class GeneralHelper
	{
        private static ILog logger = LogManager.GetLogger(typeof(GeneralHelper));

        [DllImport("kernel32.dll")]
		public static extern uint GetCurrentThreadId();

        public static UIDocument ActiveUIDocument { get; set; }

        /// <summary>
        /// Converts the bitmap to bitmap image.
        /// </summary>
        /// <param name="srcBitmap">The source GDI bitmap.</param>
        /// <returns>The DirectX bitmap image or null on error.</returns>
        public static BitmapImage BitmapToBitmapImage(Bitmap srcBitmap)
		{
			BitmapImage resultImage = null;
			using (MemoryStream mem = new MemoryStream())
			{
				srcBitmap.Save(mem, ImageFormat.Png);
				resultImage = new BitmapImage();
				resultImage.BeginInit();
				resultImage.StreamSource = new MemoryStream(mem.ToArray());
				resultImage.EndInit();
			}
			return resultImage;
		}

		/// <summary>
		/// Sets the given window's owner to Revit window.
		/// </summary>
		/// <param name="dialog">Target window.</param>
		public static void SetRevitAsWindowOwner(Window dialog)
		{
			if (null == dialog)
            {
                return;
            }

			WindowInteropHelper helper = new WindowInteropHelper(dialog);
			helper.Owner = FindRevitWindowHandle();
		}

		/// <summary>
		/// Finds the Revit window handle.
		/// </summary>
		/// <returns>Revit window handle.</returns>
		private static IntPtr FindRevitWindowHandle()
		{
			try
			{
				IntPtr foundRevitHandle = IntPtr.Zero;
				uint currentThreadID = GetCurrentThreadId();

				// Search for the Revit process with current thread ID.
				Process[] revitProcesses = Process.GetProcessesByName("Revit");
				Process foundRevitProcess = null;
				foreach (Process aRevitProcess in revitProcesses)
				{
					foreach (ProcessThread aThread in aRevitProcess.Threads)
					{
						if (aThread.Id == currentThreadID)
						{
							foundRevitProcess = aRevitProcess;
							break;
						}
					}  
                    
					if (null != foundRevitProcess)
                    {
                        break;
                    }
				}  
                
				if (null != foundRevitProcess)
				{
					foundRevitHandle = foundRevitProcess.MainWindowHandle;
				}

				return foundRevitHandle;
			}
			catch (Exception ex)
			{
                logger.Error("Error trying to find the revit window!", ex);
				return IntPtr.Zero;
			}
		}

	}
}
