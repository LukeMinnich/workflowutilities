﻿using System.Collections.Generic;

namespace RevitRemoteControl.Models
{
    public class SaveModelsPersistenceData
    {
        public string RevitFile { get; set; }

        public string SaveLocation { get; set; }

        public List<string> SelectedEntries { get; set; }
    }
}
