﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace RevitRemoteControl.WindowsStuff
{
    public class Window
    {
        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern int GetWindowTextLength(IntPtr hWnd);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern long GetWindowText(IntPtr hwnd, StringBuilder lpString, int cch);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern long GetClassName(IntPtr hwnd, StringBuilder lpClassName, int nMaxCount);

        // activate an application window:
        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern IntPtr SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int x, int Y, int cx, int cy, int wFlags);

        private delegate bool EnumWindowProc(IntPtr hwnd, IntPtr lParam);

        [DllImport("user32", SetLastError = true, CharSet = CharSet.Auto)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool EnumChildWindows(IntPtr window, EnumWindowProc callback, IntPtr lParam);

        const int HWND_TOP = 0;
        const int HWND_BOTTOM = 1;
        const int HWND_TOPMOST = -1;
        const int HWND_BOTTOMMOST = -2;

        public const int SWP_SHOWWINDOW = 0x0040;
        public const int SWP_NOSIZE = 0x0001;

        //---------------------------------------------------------------------
        private IntPtr hwnd = IntPtr.Zero;

        public IntPtr HWND {  get { return hwnd;  } }

        public bool IsOK()
        {
            return hwnd != IntPtr.Zero;
        }

        public Window(string windowName)
        {
            hwnd = FindWindow(null, windowName);
        }

        public Window(string windowName, string className)
        {
            if (className == null || windowName == null)
            {
                hwnd = FindWindowByName(windowName);
            }
            else
            {
                hwnd = FindWindow(className, windowName);
            }
        }

        public Window(IntPtr hwnd)
        {
            this.hwnd = hwnd;
        }

        public void SetPosition(float percentStartX, float percentStartY, float percentWidth, float percentHeight, int flags = SWP_SHOWWINDOW)
        {
            int width = SystemInformation.VirtualScreen.Width;
            int height = SystemInformation.VirtualScreen.Height;

            SetWindowPos(hwnd, HWND_TOP,
                (int)(percentStartX * width / 100.0),
                (int)(percentStartY * height / 100.0), 
                (int)(percentWidth * width / 100.0), 
                (int)(percentHeight * height / 100.0),
                flags);
        }

        public void SetPosition(Rectangle rect, int flags = SWP_SHOWWINDOW)
        {
            SetWindowPos(hwnd, HWND_TOP, rect.Left, rect.Top, rect.Width, rect.Height, flags);
        }

        public void SetAsForeground()
        {
            SetForegroundWindow(hwnd);
        }

        public IntPtr FindWindowByName(string s)
        {
            IntPtr hWnd = IntPtr.Zero;
            foreach (Process pList in Process.GetProcesses())
            {
                if (pList.MainWindowTitle.Length > 0)
                {
                    Debug.WriteLine("Found: " + pList.MainWindowTitle);
                }
                if (pList.MainWindowTitle.Contains(s))
                {
                    hWnd = pList.MainWindowHandle;
                }
            }
            return hWnd;
        }

        public string GetCaptionOfWindow()
        {
            string caption = "";
            StringBuilder windowText = null;
            try
            {
                int max_length = GetWindowTextLength(hwnd);
                windowText = new StringBuilder("", max_length + 5);
                GetWindowText(hwnd, windowText, max_length + 2);

                if (!String.IsNullOrEmpty(windowText.ToString()) && !String.IsNullOrWhiteSpace(windowText.ToString()))
                    caption = windowText.ToString();
            }
            catch (Exception ex)
            {
                caption = ex.Message;
            }
            finally
            {
                windowText = null;
            }
            return caption;
        }

        public string GetClassNameOfWindow()
        {
            string className = "";
            StringBuilder classText = null;
            try
            {
                int cls_max_length = 1000;
                classText = new StringBuilder("", cls_max_length + 5);
                GetClassName(hwnd, classText, cls_max_length + 2);

                if (!String.IsNullOrEmpty(classText.ToString()) && !String.IsNullOrWhiteSpace(classText.ToString()))
                    className = classText.ToString();
            }
            catch (Exception ex)
            {
                className = ex.Message;
            }
            finally
            {
                classText = null;
            }
            return className;
        }

        public List<Window> GetAllChildWindows()
        {
            List<IntPtr> childHandles = new List<IntPtr>();

            GCHandle gcChildhandlesList = GCHandle.Alloc(childHandles);
            IntPtr pointerChildHandlesList = GCHandle.ToIntPtr(gcChildhandlesList);

            try
            {
                EnumWindowProc childProc = new EnumWindowProc(EnumWindow);
                EnumChildWindows(this.hwnd, childProc, pointerChildHandlesList);
            }
            finally
            {
                gcChildhandlesList.Free();
            }

            // convert-o-rama
            List<Window> childWindows = new List<Window>();
            foreach(IntPtr ip in childHandles)
            {
                childWindows.Add(new Window(ip));
            }
            return childWindows;
        }

        private bool EnumWindow(IntPtr hWnd, IntPtr lParam)
        {
            GCHandle gcChildhandlesList = GCHandle.FromIntPtr(lParam);

            if (gcChildhandlesList == null || gcChildhandlesList.Target == null)
            {
                return false;
            }

            List<IntPtr> childHandles = gcChildhandlesList.Target as List<IntPtr>;
            childHandles.Add(hWnd);

            return true;
        }

        public string WindowDescriptor => hwnd.ToString() + ": " + GetClassNameOfWindow() + " {" + GetCaptionOfWindow() + "}";
    }
}
