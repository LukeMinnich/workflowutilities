﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace RevitRemoteControl.WindowsStuff
{
    public class Combo
    {
        [DllImport("user32.dll")]
        static extern bool GetComboBoxInfo(IntPtr hWnd, ref COMBOBOXINFO pcbi);
        
        [StructLayout(LayoutKind.Sequential)]
        public struct COMBOBOXINFO
        {
            public int cbSize;
            public RECT rcItem;
            public RECT rcButton;
            public int stateButton;
            public IntPtr hwndCombo;
            public IntPtr hwndEdit;
            public IntPtr hwndList;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int left;
            public int top;
            public int right;
            public int bottom;
        }

        IntPtr hwnd = IntPtr.Zero;
        COMBOBOXINFO info = new COMBOBOXINFO();

        public Combo(Window wnd)
        {
            info.cbSize = Marshal.SizeOf(info);
            this.hwnd = wnd.HWND;
        }

        public string GetCaption()
        {
            GetComboBoxInfo(hwnd, ref info);
            Window edit = new Window(info.hwndEdit);
            return edit.GetCaptionOfWindow();
        }

        public int GetCurrentSelection()
        {
            return (int)Messages.SendMessage(hwnd, Messages.CB_GETCURSEL, 0, null);
        }

        public int GetComboCount()
        {
            return (int)Messages.SendMessage(hwnd, Messages.CB_GETCOUNT, 0, null);
        }

        public string GetComboItem(int index)
        {
            StringBuilder ssb = new StringBuilder(256, 256);
            Messages.SendRefMessage(hwnd, Messages.CB_GETLBTEXT, index, ssb);
            return ssb.ToString();
        }

        public void SetComboItem(int index)
        {
            Messages.SendMessage(hwnd, Messages.CB_SETCURSEL, index, "0");
        }

        public void SetWindowText(string str)
        {
            Messages.SendMessage(info.hwndEdit, Messages.WM_SETTEXT, 0, str);
        }
    }
}
