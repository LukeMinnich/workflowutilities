﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace RevitRemoteControl.WindowsStuff
{
    public class Messages
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
        public static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, int wParam, string lParam);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false, EntryPoint = "SendMessage")]
        public static extern IntPtr SendRefMessage(IntPtr hWnd, uint Msg, int wParam, StringBuilder lParam);

        public static uint CB_GETLBTEXT = 0x0148;
        public static uint CB_SETCURSEL = 0x014E;
        public static uint CB_GETCURSEL = 0x0147;
        public static uint CB_GETCOUNT = 0x0146;
        public static uint CB_GETLBTEXTLEN = 0x0149;
         
        public static uint BM_CLICK = 0x00F5;
         
        public static uint WM_SETTEXT = 0x000C;
    }
}
