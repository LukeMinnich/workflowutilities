﻿using System;

namespace RevitRemoteControl.WindowsStuff
{
    public class ButtonWrapper
    {

        private IntPtr hwnd = IntPtr.Zero;

        public ButtonWrapper(Window wnd)
        {
            this.hwnd = wnd.HWND;
        }

        public void Click()
        {
            Messages.SendMessage(hwnd, Messages.BM_CLICK, 0, null);
        }
    }
}
