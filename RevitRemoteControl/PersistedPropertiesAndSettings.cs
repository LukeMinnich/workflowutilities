﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RevitRemoteControl.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RevitRemoteControl
{
    public class PersistedPropertiesAndSettings
    {
        private static PersistedPropertiesAndSettings instance;

        private PersistedPropertiesAndSettings() { }

        public static PersistedPropertiesAndSettings Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PersistedPropertiesAndSettings();
                }
                return instance;
            }
        }

        private string GetModelsMap()
        {
            return Properties.Settings.Default["ModelsMap"].ToString();
        }

        private void SetModelsMap(string persistenceData)
        {
            Properties.Settings.Default["ModelsMap"] = persistenceData;
            Properties.Settings.Default.Save(); // Saves settings in application configuration file
        }

        public SaveModelsPersistenceData GetSaveModel (string revitFile)
        {
            string json = GetModelsMap();
            List <SaveModelsPersistenceData> list = JsonConvert.DeserializeObject<List<SaveModelsPersistenceData>>(json);

            return list.Where(x => x.RevitFile == revitFile).SingleOrDefault();
        }

        public void SetSaveModel(string revitFile, SaveModelsPersistenceData persistenceData)
        {
            string json = GetModelsMap();
            List<SaveModelsPersistenceData> list = JsonConvert.DeserializeObject<List<SaveModelsPersistenceData>>(json);

            SaveModelsPersistenceData old = list.Where(x => x.RevitFile == revitFile).SingleOrDefault();
            if (old == null)
            {
                old = new SaveModelsPersistenceData();
                old.RevitFile = revitFile;
                list.Add(old);
            }

            old.SelectedEntries = persistenceData.SelectedEntries;
            old.SaveLocation = persistenceData.SaveLocation;

            SetModelsMap(JsonConvert.SerializeObject(list));
        }
    }
}
