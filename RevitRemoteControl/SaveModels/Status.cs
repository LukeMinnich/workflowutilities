﻿using log4net;
using RevitRemoteControl.WindowsStuff;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace RevitRemoteControl.SaveModels
{
    public partial class Status : Form
    {
        private static int timeOutInMinutes = 5; //TODO - get this from app.config
        private ILog logger = LogManager.GetLogger(typeof(Status));
        private System.Windows.Forms.Timer timer;

        private IRemoteSaveInterface iRemoteSaveInterface;
        private bool fClose = false;

        public List<string> filesToProcess = new List<string>();

        public Status(IRemoteSaveInterface iRemoteSaveInterface)
        {
            InitializeComponent();

            timer = new System.Windows.Forms.Timer();
            timer.Interval = 1000;
            timer.Tick += CloseForm;
            timer.Start();

            this.iRemoteSaveInterface = iRemoteSaveInterface;
            Window wnd = new Window(this.Handle);
            wnd.SetPosition(55, 5, 0, 0, Window.SWP_NOSIZE);

            // run this in a worker thread and wait for the list to be finished processing
            new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;

                AddStatusLine("Starting");
                filesToProcess.ForEach(file =>
                {
                    iRemoteSaveInterface.LaunchRevitSaveLibrariesDialog();
                    SyncSave(file);
                    return;
                });
                AddStatusLine("Done");
                Thread.Sleep(1000);

                // close the dialog
                fClose = true;
            }).Start();
        }

        private void CloseForm(object sender, EventArgs e)
        {
            if (fClose)
            {
                timer.Stop();
                timer.Dispose();
                this.DialogResult = DialogResult.OK;
            }
        }

        public void AddStatusLine(string value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string>(AddStatusLine), new object[] { value });
                return;
            }
            rtextStatus.Text += value + "\n";
        }

        public void AddStatus(string value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string>(AddStatus), new object[] { value });
                return;
            }
            rtextStatus.Text += value;
        }

        private bool SyncSave(string file)
        {
            AddStatus(file);

            logger.Info($"Working on: '{file}'");
            iRemoteSaveInterface.SelectComboItem(file);

            Thread.Sleep(1000);

            iRemoteSaveInterface.ClickSaveButton();

            string fileToWatchFor = iRemoteSaveInterface.GetFileNameWithPath(file);
            logger.Info($"Watching: '{fileToWatchFor}'");

            var timeout = DateTime.Now.Add(TimeSpan.FromMinutes(timeOutInMinutes));

            while (!File.Exists(fileToWatchFor))
            {
                if (DateTime.Now > timeout)
                {
                    logger.Warn($"File creation ran out of time: '{fileToWatchFor}' was not created within the {timeOutInMinutes} minute timeout window. I'll try moving on to the next file or terminate if need be!");
                    AddStatusLine(" falied!");
                    return false;
                }

                Thread.Sleep(TimeSpan.FromSeconds(5));
            }

            Thread.Sleep(10000); // this is arbitrary, but revit isn't done when it initially creates the file, wait a bit longer...

            string copyTheFileHere = iRemoteSaveInterface.GetDestinationWithPath(file);
            logger.Info($"Found: '{fileToWatchFor}', copying to final destination: {copyTheFileHere}");

            // To copy a folder's contents to a new location:
            // Create a new target folder, if necessary.
            if (!System.IO.Directory.Exists(Path.GetDirectoryName(copyTheFileHere)))
            {
                System.IO.Directory.CreateDirectory(Path.GetDirectoryName(copyTheFileHere));
            }

            // To copy a file to another location and 
            // overwrite the destination file if it already exists.
            System.IO.File.Copy(fileToWatchFor, copyTheFileHere, true);
            System.IO.File.Delete(fileToWatchFor);

            AddStatusLine(" success!");
            return true;
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            fClose = true;
        }
    }
}
