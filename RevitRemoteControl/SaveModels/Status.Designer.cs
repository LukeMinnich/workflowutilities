﻿namespace RevitRemoteControl.SaveModels
{
    partial class Status
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Status));
            this.rtextStatus = new System.Windows.Forms.RichTextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.bCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // rtextStatus
            // 
            this.rtextStatus.Location = new System.Drawing.Point(13, 12);
            this.rtextStatus.Name = "rtextStatus";
            this.rtextStatus.ReadOnly = true;
            this.rtextStatus.Size = new System.Drawing.Size(315, 376);
            this.rtextStatus.TabIndex = 3;
            this.rtextStatus.Text = "";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(13, 394);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(315, 99);
            this.button2.TabIndex = 4;
            this.button2.Text = resources.GetString("button2.Text");
            this.button2.UseVisualStyleBackColor = false;
            // 
            // bCancel
            // 
            this.bCancel.Location = new System.Drawing.Point(253, 506);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(75, 23);
            this.bCancel.TabIndex = 5;
            this.bCancel.Text = "Cancel";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // Status
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(340, 541);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.rtextStatus);
            this.Name = "Status";
            this.Text = "Status";
            this.ResumeLayout(false);

        }

        #endregion
        internal System.Windows.Forms.RichTextBox rtextStatus;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button bCancel;
    }
}