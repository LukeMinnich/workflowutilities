﻿using log4net;
using RevitRemoteControl.Models;
using RevitRemoteControl.WindowsStuff;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace RevitRemoteControl.SaveModels
{
    public partial class SaveModels : Form, IRemoteSaveInterface
    {
        private ILog logger = LogManager.GetLogger(typeof(SaveModels));

        private FolderBrowserDialog folderBrowserDialog1;
        private string folderName;
        private StringBuilder defaultFileLocation = new StringBuilder();
        private Combo comboGroupToSave = null;
        private ButtonWrapper btnSave = null;
        private List<ComboItem> comboBoxItems = new List<ComboItem>();
        private Window revitWindow = null;
        private string RevitFile = string.Empty;

        public SaveModels()
        {
            InitializeComponent();

            //-----------------------------------------------------------------
            // make sure that revit is running and the dialog is running/available!
            revitWindow = new Window("Autodesk Revit", null);
            RevitFile = revitWindow.WindowDescriptor;
            logger.Info("SaveModels - RevitFile: " + RevitFile);
            int iStart = RevitFile.IndexOf("STARTING VIEW - ", 0);
            int iEnd = 0;
            if (iStart > 0)
            {
                iStart += "STARTING VIEW - ".Length;
                iEnd = RevitFile.IndexOf(".rvt", iStart) + 4;
            }
            else if (RevitFile.IndexOf("Overall Working - ", 0) > 0)
            {
                iStart = RevitFile.IndexOf("Overall Working - ", 0);
                iStart += "Overall Working - ".Length;
                iEnd = RevitFile.IndexOf(".rvt", iStart) + 4;
            }
            else
            {
                iStart = RevitFile.IndexOf("Autodesk Revit 2017.2 - [", 0);
                iStart += "Autodesk Revit 2017.2 - [".Length;
                iEnd = RevitFile.IndexOf("]", iStart);
            }

            RevitFile = RevitFile.Substring(iStart, iEnd - iStart);
            this.labelRevitFileName.Text = RevitFile;
            logger.Info("SaveModels - RevitFile (shortened): " + RevitFile);

            LaunchRevitSaveLibrariesDialog();

            //-----------------------------------------------------------------
            // the dialog comes up with the default File Name as "Same as group name" - that seems right.
            // so lets grab the various combo boxes and buttons so we can instrument this dialog box...

            Window wndSaveGroupDialog = new Window("Save Group");
            logger.Info("SaveModels - wndSaveGroupDialog: " + wndSaveGroupDialog.WindowDescriptor);

            var allChildWindows = wndSaveGroupDialog.GetAllChildWindows();

            logger.Info("SaveModels - allChildWindows count: " + allChildWindows.Count);

            try
            {

                // walk through all of the children and scrape all of the information that we need.
                allChildWindows.ForEach(wnd =>
                {
                    string str = wnd.GetClassNameOfWindow();
                    logger.Info(wnd.WindowDescriptor);

                    if (str.Equals("ComboBox"))
                    {
                        Combo combo = new Combo(wnd);

                        logger.Info(combo.GetCaption());
                        int iSel = combo.GetCurrentSelection();
                        int max = combo.GetComboCount();
                        bool doDef = false;
                        for (int i = 0; i < max; i++)
                        {
                            string itemX = combo.GetComboItem(i);
                            if ((i == 0 && itemX.Equals("Desktop")) || doDef)
                            {
                                doDef = true;
                                if (i == 2)
                                {
                                    // get the Local Disk letter ALways inside a (), so look for index of '(' and get next letter
                                    int index = itemX.IndexOf('(');
                                    defaultFileLocation.Append(itemX.Substring(index + 1, 1));
                                    defaultFileLocation.Append(":");
                                }
                                else if (i > 2)
                                {
                                    if (itemX.Contains("("))
                                    {
                                        // this is terminal...
                                        doDef = false;
                                    }
                                    else
                                    {
                                        // concat into the flocation string
                                        defaultFileLocation.Append("\\");
                                        defaultFileLocation.Append(itemX);
                                    }
                                }
                            }
                            logger.Info($"-- {i} of {iSel} -- " + itemX);

                            string name = string.Empty;
                            if (itemX.StartsWith("Model Group: "))
                            {
                                comboGroupToSave = combo;
                                name = itemX;
                            }
                            else if (itemX.StartsWith("Detail Group:"))
                            {
                                comboGroupToSave = combo;
                                name = itemX;
                            }

                            if (name.Length > 0)
                            {
                                comboBoxItems.Add(new ComboItem()
                                {
                                    Name = name,
                                    Index = i
                                });
                            }
                        }
                    }
                    else if (str.Equals("Button"))
                    {
                        logger.Info(" start button");
                        string itemX = wnd.GetCaptionOfWindow();
                        if (itemX != null && itemX.Contains("Save"))
                        {
                            btnSave = new ButtonWrapper(wnd);
                        }
                        logger.Info(" button: " + itemX);
                    }
                });
            }
            catch (Exception ex)
            {
                logger.Error("Error getting windows control texts because: " + ex.Message);
            }
            logger.Info("SaveModels - done processing");

            //-----------------------------------------------------------------
            // some book keeping for the directory button!!!!!!
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.folderBrowserDialog1.Description = "Select the directory that you want to use.";
            this.folderBrowserDialog1.ShowNewFolderButton = true;
            this.folderBrowserDialog1.RootFolder = Environment.SpecialFolder.Desktop; ;

            logger.Info("SaveModels - combo box parsing.");
            comboBoxItems.ForEach(comboItem => clbModelsAvail.Items.Add(comboItem.Name));
            SaveModelsPersistenceData persData = PersistedPropertiesAndSettings.Instance.GetSaveModel(RevitFile);
            if (persData != null)
            {
                this.editExportLocation.Text = persData.SaveLocation;
                persData.SelectedEntries.ForEach(selectedIem =>
                {
                    clbModelsAvail.SetItemChecked(clbModelsAvail.FindString(selectedIem), true);
                });
            }

            // position everything appropriately
            logger.Info("SaveModels - move windows");

            Window me = new Window(this.Handle);
            me.SetPosition(65, 5, 40, 90, Window.SWP_NOSIZE | Window.SWP_SHOWWINDOW);

            revitWindow.SetPosition(5, 5, 40, 90);
        }

        private void linkAll_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            for (int i = 0; i < this.clbModelsAvail.Items.Count; i++)
            {
                this.clbModelsAvail.SetItemChecked(i, true);
            }
        }

        private void linkNone_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            for (int i = 0; i < this.clbModelsAvail.Items.Count; i++)
            {
                this.clbModelsAvail.SetItemChecked(i, false);
            }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            // Show the FolderBrowserDialog.
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                folderName = folderBrowserDialog1.SelectedPath;
                editExportLocation.Text = folderName;
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            // put a modal dialog up with status and then loop like crazy, update the dialog as we go!!!
            Status status = new Status(this);
            status.filesToProcess = clbModelsAvail.CheckedItems.Cast<string>().ToList();

            // save to persistense so these same itesm are checked next time
            SaveModelsPersistenceData persData = PersistedPropertiesAndSettings.Instance.GetSaveModel(RevitFile);
            if (persData == null)
            {
                persData = new SaveModelsPersistenceData();
            }

            persData.SelectedEntries = status.filesToProcess;
            persData.RevitFile = RevitFile;
            persData.SaveLocation = this.editExportLocation.Text;
            PersistedPropertiesAndSettings.Instance.SetSaveModel(RevitFile, persData);

            this.DialogResult = status.ShowDialog();
            Dispose();
        }

        //---------------------------------------------------------------------
        // IRemoteSaveInterface

        public bool LaunchRevitSaveLibrariesDialog()
        {
            // verify that Revit is running:
            if (!revitWindow.IsOK())
            {
                MessageBox.Show(
                    "Just can't go on!",
                    "Unable to find Revit window. Is Revit up and running yet?",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            revitWindow.SetAsForeground();
            SendKeys.SendWait("{ESC}");

            // this shortcut needs to be added by the user!!!!
            revitWindow.SetAsForeground();
            SendKeys.SendWait("s");
            revitWindow.SetAsForeground();
            SendKeys.SendWait("g");
            Thread.Sleep(1000); // give it a chance to come up!

            Window wndSaveGroupDialog = new Window("Save Group");
            logger.Info("LaunchRevitSaveLibrariesDialog - wndSaveGroupDialog: " + wndSaveGroupDialog.WindowDescriptor);

            var allChildWindows = wndSaveGroupDialog.GetAllChildWindows();
            logger.Info("LaunchRevitSaveLibrariesDialog - allChildWindows count: " + allChildWindows.Count);

            // walk through all of the children and scrape all of the information that we need.
            allChildWindows.ForEach(wnd =>
            {
                string str = wnd.GetClassNameOfWindow();

                if (str.Equals("ComboBox"))
                {
                    Combo combo = new Combo(wnd);

                    logger.Info(combo.GetCaption());
                    logger.Info("  LaunchRevitSaveLibrariesDialog combo: " + combo.GetCaption());
                    int iSel = combo.GetCurrentSelection();
                    int max = combo.GetComboCount();
                    if (max > 0)
                    {
                        string itemX = combo.GetComboItem(0);

                        if (itemX.StartsWith("Model Group: ") || itemX.StartsWith("Detail Group:"))
                        {
                            comboGroupToSave = combo;
                            logger.Info("  LaunchRevitSaveLibrariesDialog comboGroupToSave set to: " + itemX);
                        }
                    }
                }
                else if (str.Equals("Button"))
                {
                    string itemX = wnd.GetCaptionOfWindow();
                    logger.Info("  LaunchRevitSaveLibrariesDialog button: " + itemX);
                    if (itemX.Contains("Save"))
                    {
                        btnSave = new ButtonWrapper(wnd);
                        logger.Info("  LaunchRevitSaveLibrariesDialog btnSave set to: " + itemX);
                    }
                }
            });
            return true;
        }

        public bool SelectComboItem(string file)
        {
            ComboItem ci = comboBoxItems.Where(x => x.Name.Equals(file)).SingleOrDefault();

            if (ci.Index >= 0)
            {
                comboGroupToSave.SetComboItem(ci.Index);
                return true;
            }
            else
            {
                return false;
            }
        }

        public void ClickSaveButton()
        {
            btnSave.Click();
        }

        public string GetFileNameWithPath(string fileNamePlus)
        {
            string name = string.Empty;
            if (fileNamePlus.StartsWith("Model Group: "))
            {
                name = fileNamePlus.Substring("Model Group: ".Length);
            }
            else if (fileNamePlus.StartsWith("Detail Group:"))
            {
                name = fileNamePlus.Substring("Detail Group:".Length);
            }

            name = name.Replace('.', '-');
            return defaultFileLocation + "\\" + name + ".rvt";
        }

        public string GetDestinationWithPath(string fileNamePlus)
        {
            string name = string.Empty;
            if (fileNamePlus.StartsWith("Model Group: "))
            {
                name = fileNamePlus.Substring("Model Group: ".Length);
            }
            else if (fileNamePlus.StartsWith("Detail Group:"))
            {
                name = fileNamePlus.Substring("Detail Group:".Length);
            }

            // note that we don't replace the inner '.'s like above.
            return editExportLocation.Text + "\\" + name + ".rvt";
        }

    }
}
