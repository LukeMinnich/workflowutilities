﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevitRemoteControl.SaveModels
{
    public interface IRemoteSaveInterface
    {
        bool LaunchRevitSaveLibrariesDialog();

        bool SelectComboItem(string file);

        void ClickSaveButton();

        string GetFileNameWithPath(string fileNamePlus);

        string GetDestinationWithPath(string fileNamePlus);
    }
}
