﻿namespace RevitRemoteControl.SaveModels
{
    partial class SaveModels
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnStart = new System.Windows.Forms.Button();
            this.linkAll = new System.Windows.Forms.LinkLabel();
            this.linkNone = new System.Windows.Forms.LinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.clbModelsAvail = new System.Windows.Forms.CheckedListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.editExportLocation = new System.Windows.Forms.TextBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.labelRevitFileName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(244, 740);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 1;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // linkAll
            // 
            this.linkAll.AutoSize = true;
            this.linkAll.Location = new System.Drawing.Point(12, 58);
            this.linkAll.Name = "linkAll";
            this.linkAll.Size = new System.Drawing.Size(18, 13);
            this.linkAll.TabIndex = 4;
            this.linkAll.TabStop = true;
            this.linkAll.Text = "All";
            this.linkAll.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkAll_LinkClicked);
            // 
            // linkNone
            // 
            this.linkNone.AutoSize = true;
            this.linkNone.Location = new System.Drawing.Point(37, 58);
            this.linkNone.Name = "linkNone";
            this.linkNone.Size = new System.Drawing.Size(33, 13);
            this.linkNone.TabIndex = 5;
            this.linkNone.TabStop = true;
            this.linkNone.Text = "None";
            this.linkNone.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkNone_LinkClicked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 20);
            this.label1.TabIndex = 6;
            this.label1.Text = "Models available for:";
            // 
            // clbModelsAvail
            // 
            this.clbModelsAvail.FormattingEnabled = true;
            this.clbModelsAvail.Location = new System.Drawing.Point(12, 74);
            this.clbModelsAvail.Name = "clbModelsAvail";
            this.clbModelsAvail.Size = new System.Drawing.Size(307, 574);
            this.clbModelsAvail.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 676);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Export Location:";
            // 
            // editExportLocation
            // 
            this.editExportLocation.Location = new System.Drawing.Point(110, 673);
            this.editExportLocation.Name = "editExportLocation";
            this.editExportLocation.Size = new System.Drawing.Size(212, 20);
            this.editExportLocation.TabIndex = 11;
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(110, 700);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 12;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // labelRevitFileName
            // 
            this.labelRevitFileName.AutoSize = true;
            this.labelRevitFileName.Location = new System.Drawing.Point(16, 33);
            this.labelRevitFileName.Name = "labelRevitFileName";
            this.labelRevitFileName.Size = new System.Drawing.Size(98, 13);
            this.labelRevitFileName.TabIndex = 13;
            this.labelRevitFileName.Text = "labelRevitFileName";
            // 
            // SaveModels
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(331, 775);
            this.Controls.Add(this.labelRevitFileName);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.editExportLocation);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.clbModelsAvail);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.linkNone);
            this.Controls.Add(this.linkAll);
            this.Controls.Add(this.btnStart);
            this.Name = "SaveModels";
            this.Text = "SaveModels";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.LinkLabel linkAll;
        private System.Windows.Forms.LinkLabel linkNone;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckedListBox clbModelsAvail;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox editExportLocation;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.Label labelRevitFileName;
    }
}