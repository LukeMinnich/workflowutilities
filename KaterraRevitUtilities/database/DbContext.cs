﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Data;

namespace KaterraUtilities.database
{
    public class DbContext : IDisposable
    {
        public delegate T MyRowFiller<T>(MySqlDataReader dataReader);

        private MySqlConnection sqlConnection;

        public DbContext(string connection)
        {
            Initialize(connection);
        }

        private void Initialize(string connection)
        {
            sqlConnection = new MySqlConnection(connection);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
        }
        
        private bool OpenConnection()
        {
            try
            {
                sqlConnection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                switch (ex.Number)
                {
                    case 0:
                        Debug.WriteLine("Cannot connect to server.  Contact administrator");
                        break;

                    case 1045:
                        Debug.WriteLine("Invalid username/password, please try again");
                        break;

                    default:
                        Debug.WriteLine("Unknown error: " + ex.ToString());
                        break;
                }
                return false;
            }
        }

        private bool CloseConnection()
        {
            try
            {
                sqlConnection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }

        //---------------------------------------------------------------------
        // the real db call wrappers...

        public int ExecuteNonQuery(string query, Dictionary<string, object> parameters = null)
        {
            int ret = -1;
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, sqlConnection);
                AddParameters(cmd, parameters);
                ret = cmd.ExecuteNonQuery();
                this.CloseConnection();
            }
            return ret;
        }

        public IEnumerable<T> ExecuteQuery<T>(string query, MyRowFiller<T> filler, Dictionary<string, object> parameters = null)
        {
            List<T> list = new List<T>();
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, sqlConnection);
                AddParameters(cmd, parameters);
                MySqlDataReader dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    list.Add(filler(dataReader));
                }
                dataReader.Close();
                this.CloseConnection();
                 return list;
            }
            else
            {
                return list;
            }
        }

        public void ScalarVoid(string query, Dictionary<string, object> parameters = null)
        {
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, sqlConnection);
                AddParameters(cmd, parameters);
                cmd.ExecuteScalar();
                this.CloseConnection();
            }
        }

        public int ScalarInt(string query, Dictionary<string, object> parameters = null)
        {
            int ret = -1;
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, sqlConnection);
                AddParameters(cmd, parameters);
                ret = int.Parse(cmd.ExecuteScalar() + "");
                this.CloseConnection();
            }
            return ret;
        }

        private void AddParameters(MySqlCommand cmd, Dictionary<string, object> parameters)
        {
            if (parameters == null)
                return;

            foreach (KeyValuePair<string, object> kvp in parameters)
            {
                cmd.Parameters.AddWithValue(kvp.Key, kvp.Value);
            }
        }
    }
}
