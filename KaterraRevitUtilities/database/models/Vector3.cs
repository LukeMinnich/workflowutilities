﻿using Autodesk.Revit.DB;
using System;
using System.ComponentModel;
using Math = System.Math;

namespace KaterraUtilities.database.models
{
    [ImmutableObject(true), Serializable]
    public struct Vector3 : IFormattable
    {
        private readonly double x;
        private readonly double y;
        private readonly double z;

        public static readonly Vector3 Origin = new Vector3(0, 0, 0);
        public static readonly Vector3 XAxis = new Vector3(1, 0, 0);
        public static readonly Vector3 YAxis = new Vector3(0, 1, 0);
        public static readonly Vector3 ZAxis = new Vector3(0, 0, 1);

        public Vector3(double x, double y, double z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public Vector3(Vector3 v1)
        {
            this.x = v1.X;
            this.y = v1.Y;
            this.z = v1.Z;
        }

        public Vector3(XYZ v1)
        {
            this.x = v1.X;
            this.y = v1.Y;
            this.z = v1.Z;
        }

        public double X { get { return this.x; } }
        public double Y { get { return this.y; } }
        public double Z { get { return this.z; } }

        public static explicit operator XYZ(Vector3 v1)
        {
            return new XYZ(v1.X, v1.Y, v1.Z);
        }

        public static Vector3 operator +(Vector3 v1, Vector3 v2)
        {
            return new Vector3(
                v1.X + v2.X,
                v1.Y + v2.Y,
                v1.Z + v2.Z);
        }

        public static Vector3 operator -(Vector3 v1, Vector3 v2)
        {
            return new Vector3(
                v1.X - v2.X,
                v1.Y - v2.Y,
                v1.Z - v2.Z);
        }

        public static Vector3 operator *(Vector3 v1, double s2)
        {
            return
                new Vector3(
                        v1.X * s2,
                        v1.Y * s2,
                        v1.Z * s2);
        }

        public static Vector3 operator *(double s1, Vector3 v2)
        {
            return v2 * s1;
        }

        public static Vector3 operator /(Vector3 v1, double s2)
        {
            return new Vector3(
                        v1.X / s2,
                        v1.Y / s2,
                        v1.Z / s2);
        }

        public static Vector3 operator -(Vector3 v1)
        {
            return new Vector3(
                -v1.X,
                -v1.Y,
                -v1.Z);
        }

        public static Vector3 operator +(Vector3 v1)
        {
            return new Vector3(
                +v1.X,
                +v1.Y,
                +v1.Z);
        }

        public static double SumComponents(Vector3 v1)
        {
            return v1.X + v1.Y + v1.Z;
        }

        public double SumComponents()
        {
            return SumComponents(this);
        }

        public static double SumComponentSqrs(Vector3 v1)
        {
            Vector3 v2 = SqrComponents(v1);
            return v2.SumComponents();
        }

        public double SumComponentSqrs()
        {
            return SumComponentSqrs(this);
        }

        public static Vector3 PowComponents(Vector3 v1, double power)
        {
            return new Vector3(
                Math.Pow(v1.X, power),
                Math.Pow(v1.Y, power),
                Math.Pow(v1.Z, power));
        }

        public Vector3 PowComponents(double power)
        {
            return PowComponents(this, power);
        }

        public static Vector3 SqrtComponents(Vector3 v1)
        {
            return new Vector3(
                Math.Sqrt(v1.X),
                Math.Sqrt(v1.Y),
                Math.Sqrt(v1.Z));
        }

        public Vector3 SqrtComponents()
        {
            return SqrtComponents(this);
        }

        public static Vector3 SqrComponents(Vector3 v1)
        {
            return new Vector3(
                v1.X * v1.X,
                v1.Y * v1.Y,
                v1.Z * v1.Z);
        }

        public Vector3 SqrComponents()
        {
            return SqrComponents(this);
        }

        public override string ToString()
        {
            return this.ToString(null, null);
        }

        public string ToString(string format, IFormatProvider formatProvider)
        {
            // If no format is passed
            if (format == null || format == "")
            {
                return string.Format("({0}, {1}, {2})", this.X, this.Y, this.Z);
            }

            char firstChar = format[0];
            string remainder = null;

            if (format.Length > 1)
            {
                remainder = format.Substring(1);
            }

            switch (firstChar)
            {
                case 'x': return this.X.ToString(remainder, formatProvider);
                case 'y': return this.Y.ToString(remainder, formatProvider);
                case 'z': return this.Z.ToString(remainder, formatProvider);
                default:
                    return String.Format(
                        "({0}, {1}, {2})",
                        this.X.ToString(format, formatProvider),
                        this.Y.ToString(format, formatProvider),
                        this.Z.ToString(format, formatProvider));
            }
        }
    }
}
