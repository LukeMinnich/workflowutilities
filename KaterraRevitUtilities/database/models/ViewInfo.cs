﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaterraUtilities.database.models
{
    public class ViewInfo
    {
        public int id { get; set; }
        public int idMasterFile { get; set; }
        public string name { get; set; }
    }
}
