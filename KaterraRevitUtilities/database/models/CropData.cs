﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaterraUtilities.database.models
{
    public class CropData
    {
        public int id { get; set; }
        public int idView { get; set; }
        public string name { get; set; }
        public string parent { get; set; }
        public string cropRegionJSON { get; set; }
        
        public double bottomOffset { get; set; }
        public double topOffset { get; set; }
        public double rightOffset { get; set; }
        public double leftOffset { get; set; }
    }
}
