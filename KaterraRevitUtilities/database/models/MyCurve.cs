﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace KaterraUtilities.database.models
{
    public class MyCurve
    {
        [JsonProperty("Origin")]
        public Vector3 Origin { get; set; }

        [JsonProperty("EndPoint")]
        public Vector3 EndPoint { get; set; }
    }

    public abstract class JsonCreationConverter<T> : JsonConverter
    {
        protected abstract T Create(Type objectType, JObject jsonObject);

        public override bool CanConvert(Type objectType)
        {
            return typeof(T).IsAssignableFrom(objectType);
        }

        public override object ReadJson(JsonReader reader, Type objectType,
          object existingValue, JsonSerializer serializer)
        {
            var jsonObject = JObject.Load(reader);
            var target = Create(objectType, jsonObject);
            serializer.Populate(jsonObject.CreateReader(), target);
            return target;
        }

        public override void WriteJson(JsonWriter writer, object value,
       JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }

    public class JsonVector3Converter : JsonCreationConverter<Vector3>
    {
        protected override Vector3 Create(Type objectType, JObject jsonObject)
        {
            return new Vector3((double)jsonObject["X"], (double)jsonObject["Y"], (double)jsonObject["Z"]);
        }
    }

    public class JsonMyCurveConverter : JsonCreationConverter<MyCurve>
    {
        protected override MyCurve Create(Type objectType, JObject jsonObject)
        {
            return new MyCurve();
        }
    }
}
