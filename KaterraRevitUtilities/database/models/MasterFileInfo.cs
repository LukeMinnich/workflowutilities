﻿using System;

namespace KaterraUtilities.database.models
{
    public class MasterFileInfo
    {
        public int id { get; set; }

        public string name { get; set; }

        public string comment { get; set; }

        public DateTime dt { get; set; }
    }
}
