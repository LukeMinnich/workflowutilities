﻿using Autodesk.Revit.DB;
using KaterraUtilities.database;
using KaterraUtilities.database.models;
using KaterraUtilities.Extensions;
using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;

namespace KaterraUtilities.Dependents
{
    public partial class SaveDependentViews : System.Windows.Forms.Form
    {
        private static ILog logger = LogManager.GetLogger(typeof(SaveDependentViews));

        private string masterFileName;
        private Dictionary<string, List<CropData>> depViews = new Dictionary<string, List<CropData>>();

        public SaveDependentViews(Document curDoc)
        {
            InitializeComponent();

            masterFileName = curDoc.Title;

            FillTreeWithViews(curDoc);
        }

        private void FillTreeWithViews(Document curDoc)
        {
            depViews = curDoc.GetDependentViews();
            
            foreach (KeyValuePair<string, List<CropData>> kvp in depViews)
            {
                string key = kvp.Key;

                var temp = new TreeNode(key, 1, 1);
                temp.Name = key;
                temp.Expand();
                treeOfViewTypes.Nodes.Add(temp);
                var idxRoot = treeOfViewTypes.Nodes.IndexOfKey(key);

                foreach (CropData cd in kvp.Value)
                {
                    Debug.WriteLine(key + ": " + cd.name);

                    var tempChild = new TreeNode(cd.name, 1, 1);
                    tempChild.Expand();
                    tempChild.Name = cd.name;
                    treeOfViewTypes.Nodes[idxRoot].Nodes.Add(tempChild);
                }
            }
        }

        private void ExpandNode(string nodeText)
        {
            // expand the top level views 
            foreach (TreeNode node in treeOfViewTypes.Nodes)
            {
                string strType = node.Name;
                if (strType.Equals(nodeText))
                {
                    node.Expand();
                }
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                string connectionString = Config.I["ConnectionString"];
                using (DbContext db = new DbContext(connectionString))
                {
                    string sql = $"INSERT INTO MasterFileInfo (name, comment) VALUES (@name, @comment)"; 
                    Dictionary<string, object> parameters = new Dictionary<string, object>();
                    parameters.Add("@name", masterFileName);
                    parameters.Add("@comment", tbComment.Text);

                    //logger.Info($"* {sql}");
                    db.ExecuteNonQuery(sql, parameters);
                    int idMasterFile = db.ScalarInt("select last_insert_id()");

                    foreach (KeyValuePair<string, List<CropData>> kvp in depViews)
                    {
                        // add entry for View
                        sql = $"INSERT INTO ViewInfo (idMasterFile, name) VALUES(@idMasterFile, @name)"; 
                        Dictionary<string, object> parameters2 = new Dictionary<string, object>();
                        parameters2.Add("@name", kvp.Key);
                        parameters2.Add("@idMasterFile", idMasterFile);

                        //logger.Info($"* * {sql}");
                        db.ExecuteNonQuery(sql, parameters2);
                        int idView = db.ScalarInt("select last_insert_id()");
                        foreach (CropData cd in kvp.Value)
                        {
                            // add depviews
                            sql = "INSERT INTO cropInfo (idView, name, parent, cropRegionJSON, bottomOffset, topOffset, rightOffset, leftOffset) " +
                                $"VALUES(@idView, @name, @parent, @cropRegionJSON, @bottomOffset, @topOffset, @rightOffset, @leftOffset)";
                            Dictionary<string, object> parameters3 = new Dictionary<string, object>();
                            parameters3.Add("@idView", idView);
                            parameters3.Add("@name", cd.name);
                            parameters3.Add("@parent", cd.parent);
                            parameters3.Add("@cropRegionJSON", cd.cropRegionJSON);
                            parameters3.Add("@bottomOffset", cd.bottomOffset);
                            parameters3.Add("@topOffset", cd.topOffset);
                            parameters3.Add("@rightOffset", cd.rightOffset);
                            parameters3.Add("@leftOffset", cd.leftOffset);

                            //logger.Info($"* * * {sql}");
                            db.ExecuteNonQuery(sql, parameters3);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error($"someone threw an exception in btnOK_Click", ex);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            // not really anything to do except maybe log that they cancled...
        }
    }
}
