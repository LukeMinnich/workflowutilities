﻿using System.Windows.Forms;

namespace KaterraUtilities.Dependents
{
    partial class SyncDependentViews
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SyncDependentViews));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.m_buttonBack = new System.Windows.Forms.Button();
            this.m_buttonFinish = new System.Windows.Forms.Button();
            this.m_buttonNext = new System.Windows.Forms.Button();
            this.m_panelContainer = new System.Windows.Forms.Panel();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lvMasterFileDates = new System.Windows.Forms.ListView();
            this.tbLinkFile = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tvCreate = new System.Windows.Forms.TreeView();
            this.tvSync = new System.Windows.Forms.TreeView();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbViewsFor = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.rtResults = new System.Windows.Forms.RichTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.m_labelSubtitle = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.m_panelContainer.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.tableLayoutPanel1, "tableLayoutPanel1");
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.m_panelContainer, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.m_buttonBack);
            this.panel2.Controls.Add(this.m_buttonFinish);
            this.panel2.Controls.Add(this.m_buttonNext);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // m_buttonBack
            // 
            resources.ApplyResources(this.m_buttonBack, "m_buttonBack");
            this.m_buttonBack.BackColor = System.Drawing.Color.Lavender;
            this.m_buttonBack.Name = "m_buttonBack";
            this.m_buttonBack.UseVisualStyleBackColor = false;
            this.m_buttonBack.Click += new System.EventHandler(this.m_buttonBack_Click);
            // 
            // m_buttonFinish
            // 
            resources.ApplyResources(this.m_buttonFinish, "m_buttonFinish");
            this.m_buttonFinish.BackColor = System.Drawing.Color.Lavender;
            this.m_buttonFinish.Name = "m_buttonFinish";
            this.m_buttonFinish.UseVisualStyleBackColor = false;
            this.m_buttonFinish.Click += new System.EventHandler(this.m_buttonFinish_Click);
            // 
            // m_buttonNext
            // 
            resources.ApplyResources(this.m_buttonNext, "m_buttonNext");
            this.m_buttonNext.BackColor = System.Drawing.Color.Lavender;
            this.m_buttonNext.Name = "m_buttonNext";
            this.m_buttonNext.UseVisualStyleBackColor = false;
            this.m_buttonNext.Click += new System.EventHandler(this.m_buttonNext_Click);
            // 
            // m_panelContainer
            // 
            this.m_panelContainer.Controls.Add(this.tabControl);
            resources.ApplyResources(this.m_panelContainer, "m_panelContainer");
            this.m_panelContainer.Name = "m_panelContainer";
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.tabPage2);
            this.tabControl.Controls.Add(this.tabPage3);
            resources.ApplyResources(this.tabControl, "tabControl");
            this.tabControl.HotTrack = true;
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tableLayoutPanel2);
            resources.ApplyResources(this.tabPage1, "tabPage1");
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            resources.ApplyResources(this.tableLayoutPanel2, "tableLayoutPanel2");
            this.tableLayoutPanel2.Controls.Add(this.panel3, 0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            // 
            // panel3
            // 
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Controls.Add(this.lvMasterFileDates);
            this.panel3.Controls.Add(this.tbLinkFile);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Name = "panel3";
            // 
            // lvMasterFileDates
            // 
            this.lvMasterFileDates.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.lvMasterFileDates.CausesValidation = false;
            this.lvMasterFileDates.FullRowSelect = true;
            this.lvMasterFileDates.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvMasterFileDates.HideSelection = false;
            resources.ApplyResources(this.lvMasterFileDates, "lvMasterFileDates");
            this.lvMasterFileDates.MultiSelect = false;
            this.lvMasterFileDates.Name = "lvMasterFileDates";
            this.lvMasterFileDates.UseCompatibleStateImageBehavior = false;
            this.lvMasterFileDates.View = System.Windows.Forms.View.Details;
            // 
            // tbLinkFile
            // 
            this.tbLinkFile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.tbLinkFile, "tbLinkFile");
            this.tbLinkFile.Name = "tbLinkFile";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tableLayoutPanel3);
            resources.ApplyResources(this.tabPage2, "tabPage2");
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            resources.ApplyResources(this.tableLayoutPanel3, "tableLayoutPanel3");
            this.tableLayoutPanel3.Controls.Add(this.panel4, 0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            // 
            // panel4
            // 
            resources.ApplyResources(this.panel4, "panel4");
            this.panel4.Controls.Add(this.tvCreate);
            this.panel4.Controls.Add(this.tvSync);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.tbViewsFor);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Name = "panel4";
            // 
            // tvCreate
            // 
            this.tvCreate.CheckBoxes = true;
            resources.ApplyResources(this.tvCreate, "tvCreate");
            this.tvCreate.Name = "tvCreate";
            this.tvCreate.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.tvCreate_AfterCheck);
            // 
            // tvSync
            // 
            this.tvSync.CheckBoxes = true;
            resources.ApplyResources(this.tvSync, "tvSync");
            this.tvSync.Name = "tvSync";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // tbViewsFor
            // 
            this.tbViewsFor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.tbViewsFor, "tbViewsFor");
            this.tbViewsFor.Name = "tbViewsFor";
            this.tbViewsFor.ReadOnly = true;
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.tableLayoutPanel4);
            resources.ApplyResources(this.tabPage3, "tabPage3");
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            resources.ApplyResources(this.tableLayoutPanel4, "tableLayoutPanel4");
            this.tableLayoutPanel4.Controls.Add(this.panel5, 0, 0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.rtResults);
            resources.ApplyResources(this.panel5, "panel5");
            this.panel5.Name = "panel5";
            // 
            // rtResults
            // 
            resources.ApplyResources(this.rtResults, "rtResults");
            this.rtResults.Name = "rtResults";
            this.rtResults.ReadOnly = true;
            this.rtResults.ShortcutsEnabled = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.m_labelSubtitle);
            this.panel1.Controls.Add(this.label1);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // m_labelSubtitle
            // 
            resources.ApplyResources(this.m_labelSubtitle, "m_labelSubtitle");
            this.m_labelSubtitle.Name = "m_labelSubtitle";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // SyncDependentViews
            // 
            this.AcceptButton = this.m_buttonFinish;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "SyncDependentViews";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.m_panelContainer.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        public System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public TabControl tabControl;
        public System.Windows.Forms.Panel panel2;
        public Button m_buttonBack;
        public Button m_buttonNext;
        public Button m_buttonFinish;
        public System.Windows.Forms.Panel m_panelContainer;
        public System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel1;
        private Label m_labelSubtitle;
        private Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel3;
        private Label label2;
        private TabPage tabPage2;
        private TableLayoutPanel tableLayoutPanel3;
        private Panel panel4;
        private TabPage tabPage3;

        #endregion
        private ListView lvMasterFileDates;
        private TextBox tbLinkFile;
        private Label label3;
        private TreeView tvCreate;
        private Label label5;
        private Label label4;
        private TreeView tvSync;
        private TextBox tbViewsFor;
        private Label label7;
        private TableLayoutPanel tableLayoutPanel4;
        private Panel panel5;
        private RichTextBox rtResults;
    }
}