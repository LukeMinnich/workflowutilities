﻿using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.DB;
using log4net;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace KaterraUtilities.Dependents
{
    public static class BatchDependentActions
    {
        private static ILog logger = LogManager.GetLogger(typeof(BatchDependentActions));

        public static void BatchDuplicateViews(Document curDoc, bool fSave = true)
        {
            if (fSave)
                Save(curDoc);
            else
                Sync(curDoc);
        }

        private static void Save(Document curDoc)
        {
            using (SaveDependentViews curForm = new SaveDependentViews(curDoc))
            {
                curForm.ShowDialog();
            }
        }

        private static void Sync(Document curDoc)
        {
            string s = GetAllRevitLinkInstances(curDoc);
            if (s != null && s.Length > 1)
            {
                using (SyncDependentViews curForm = new SyncDependentViews(curDoc, s))
                {
                    curForm.ShowDialog();
                }
            }
            else
            {
                MessageBox.Show("This file is not linked to another or the other links cannot be found!");
            }
        }

        public static string GetAllRevitLinkInstances(Document doc)
        {
            FilteredElementCollector collector = new FilteredElementCollector(doc);
            collector.OfClass(typeof(RevitLinkInstance));
            StringBuilder linkedDocs = new StringBuilder();
            foreach (Element elem in collector)
            {
                RevitLinkInstance instance = elem as RevitLinkInstance;
                Document linkDoc = instance.GetLinkDocument();
                if (linkDoc != null)
                {
                    linkedDocs.Append(linkDoc.Title);
                    //RevitLinkType type = doc.GetElement(instance.GetTypeId()) as RevitLinkType;
                    //linkedDocs.AppendLine("Is Nested: " + type.IsNestedLink.ToString());
                }
            }

            return linkedDocs.ToString();
        }
    }
}
