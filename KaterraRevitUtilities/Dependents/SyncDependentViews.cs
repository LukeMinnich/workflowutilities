﻿using Autodesk.Revit.DB;
using KaterraUtilities.database;
using KaterraUtilities.database.models;
using KaterraUtilities.Extensions;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using static KaterraUtilities.database.DbContext;

namespace KaterraUtilities.Dependents
{
    public partial class SyncDependentViews : System.Windows.Forms.Form
    {
        private static ILog logger = LogManager.GetLogger(typeof(SyncDependentViews));

        private int currentPanelIndex;
        private string linkFile;
        private Document curDoc;
        private Dictionary<string, List<CropData>> depViews = new Dictionary<string, List<CropData>>();

        public SyncDependentViews(Document curDoc, string linkFile)
        {
            InitializeComponent();
            int x = this.lvMasterFileDates.Width / 3 == 0 ? 1 : lvMasterFileDates.Width / 3;
            this.lvMasterFileDates.Columns.Add("Date", x, HorizontalAlignment.Left);
            this.lvMasterFileDates.Columns.Add("Comment", 2 * x, HorizontalAlignment.Left);
            this.lvMasterFileDates.FullRowSelect = true;

            this.linkFile = linkFile;
            m_panelContainer.Controls.Remove(tabControl);
            currentPanelIndex = -1;
            SetCurrentTab(0);
            this.curDoc = curDoc;

            depViews = curDoc.GetDependentViews();

            m_buttonBack.Visible = false;
            m_buttonNext.Visible = true;
            m_buttonFinish.Visible = false;
            m_buttonFinish.SendToBack();
        }

        private void SetCurrentTab(int currentIndex)
        {
            int count;
            if (currentPanelIndex > -1) //remove all old controls back to tabcontrol
            {
                count = m_panelContainer.Controls.Count;

                for (int i = count - 1; i >= 0; i--)
                {
                    System.Windows.Forms.Control control = m_panelContainer.Controls[i];
                    m_panelContainer.Controls.Remove(control);
                    tabControl.TabPages[currentPanelIndex].Controls.Add(control);
                }
            }

            count = tabControl.TabPages[currentIndex].Controls.Count;
            m_labelSubtitle.Text = (currentIndex + 1) + " : " + tabControl.TabPages[currentIndex].Text;
            for (int i = count - 1; i >= 0; i--)
            {
                System.Windows.Forms.Control control = tabControl.TabPages[currentIndex].Controls[i];
                tabControl.TabPages[currentIndex].Controls.Remove(control);
                m_panelContainer.Controls.Add(control);
            }

            currentPanelIndex = currentIndex;
            populate(currentIndex);
        }

        private List<MasterFileInfo> masterFileItems = null;
        private List<ViewInfo> viewInfo = null;
        private Dictionary<string, CropData> cropData = new Dictionary<string, CropData>();
        private string results = string.Empty;
        public MyRowFiller<CropData> cdRowFiller = (reader) =>
        {
            CropData cd = new CropData();
            cd.name = reader.GetString("name");
            cd.parent = reader.GetString("parent");
            cd.idView = reader.GetInt32("idView");
            cd.id = reader.GetInt32("id");
            cd.cropRegionJSON = reader.GetString("cropRegionJSON");
            cd.bottomOffset = reader.GetDouble("bottomOffset");
            cd.topOffset = reader.GetDouble("topOffset");
            cd.rightOffset = reader.GetDouble("rightOffset");
            cd.leftOffset = reader.GetDouble("leftOffset");
            return cd;
        };

        private void populate(int index)
        {
            // get data from the database and populate the controls accordingly.
            string connectionString = Config.I["ConnectionString"];
            switch (index)
            {
                case 0:
                    {
                        tbLinkFile.Text = linkFile;
                        if (masterFileItems != null)
                        {
                            masterFileItems.Clear();
                            lvMasterFileDates.Clear();
                        }
                        using (DbContext db = new DbContext(connectionString))
                        {
                            MyRowFiller<MasterFileInfo> rowFiller = (reader) =>
                            {
                                MasterFileInfo mfi = new MasterFileInfo();
                                mfi.id = reader.GetInt32("id");
                                mfi.comment = reader.GetString("comment");
                                mfi.name = reader.GetString("name");
                                mfi.dt = reader.GetDateTime("dt");

                                return mfi;
                            };

                            masterFileItems = db.ExecuteQuery(
                                $"select * from masterFileInfo where name = '{linkFile}' order by 4 desc",
                                rowFiller).ToList();

                            foreach (MasterFileInfo mfi in masterFileItems)
                            {
                                lvMasterFileDates.Items.Add(mfi.dt.ToString()).SubItems.Add(mfi.comment);
                            }

                            if (masterFileItems.Count > 0)
                                lvMasterFileDates.Items[0].Selected = true;
                        }
                    }
                    break;

                case 1:
                    {
                        if (viewInfo != null)
                        {
                            viewInfo.Clear();
                            cropData.Clear();
                            tvSync.Nodes.Clear();
                            tvCreate.Nodes.Clear();
                        }
                        String dateSelected = lvMasterFileDates.SelectedItems[0].SubItems[0].Text;
                        MasterFileInfo mfi = masterFileItems.Where(x => x.dt.ToString() == dateSelected).FirstOrDefault();
                        int idMasterFile = mfi.id;

                        tbViewsFor.Text = linkFile + " (" + dateSelected + ")";

                        using (DbContext db = new DbContext(connectionString))
                        {
                            MyRowFiller<ViewInfo> rowFiller = (reader) =>
                            {
                                ViewInfo vi = new ViewInfo();
                                vi.name = reader.GetString("name");
                                vi.id = reader.GetInt32("id");
                                return vi;
                            };

                            viewInfo = db.ExecuteQuery(
                                $"select * from viewinfo where idMasterFile = '{idMasterFile}' order by 3 asc",
                                rowFiller).ToList();

                            foreach (ViewInfo vi in viewInfo)
                            {
                                TreeNode node;
                                if (depViews.ContainsKey(vi.name))
                                {
                                    node = tvSync.Nodes.Add(vi.name);
                                    node.Checked = true;
                                }
                                else
                                {
                                    node = tvCreate.Nodes.Add(vi.name);
                                }

                                List<CropData> cropDataLocal = db.ExecuteQuery(
                                    $"select * from cropinfo where idView = '{vi.id}' order by 3 asc",
                                    cdRowFiller).ToList();

                                foreach (CropData cd in cropDataLocal)
                                {
                                    cropData.Add(cd.name, cd);
                                    var tempChild = new TreeNode(cd.name, 1, 1);
                                    if (node.Checked)
                                        tempChild.Checked = true;
                                    node.Nodes.Add(tempChild);
                                }
                                node.Expand();
                            }
                        }
                    }
                    break;

                case 2:
                    {

                    }
                    break;
            }
        }

        bool NextButtonClicked(out string reasonMessage)
        {
            //Return true if condition is satisfied. Otherwise return false. In case of false, the next page will not be shown. An error message should be assigned to show an error.
            if (currentPanelIndex == 1)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine(linkFile + " linked file summary\n");

                // sync work first!
                // all the data is in cropData, pair this with the selected items in tvSync and 
                // update the appropriate views
                foreach (TreeNode node in tvSync.Nodes)
                {
                    if (!node.Checked)
                        continue;

                    string name = node.Text;
                    string connectionString = Config.I["ConnectionString"];
                    using (DbContext db = new DbContext(connectionString))
                    {
                        // now go through dependent views and see which ones are checked...
                        foreach (TreeNode childNode in node.Nodes)
                        {
                            if (!childNode.Checked)
                                continue;

                            CropData cd = cropData[childNode.Text];
                            if (cd != null)
                            {
                                Autodesk.Revit.DB.View vw = curDoc.GetViewByName(cd.name);

                                if (vw != null)
                                    UpdateCrops(" => updated sucessfully", curDoc, vw, cd, sb);
                            }
                        }
                    }
                }

                // step 2 is to create new dependent views if any are selected in tvCreate
                sb.AppendLine("");
                foreach (TreeNode node in tvCreate.Nodes)
                {
                    if (!node.Checked)
                        continue;

                    string name = node.Text;
                    string connectionString = Config.I["ConnectionString"];
                    using (DbContext db = new DbContext(connectionString))
                    {
                        // now go through dependent views and see which ones are checked...
                        foreach (TreeNode childNode in node.Nodes)
                        {
                            if (!childNode.Checked)
                                continue;

                            CropData cd = cropData[childNode.Text];
                            if (cd != null)
                            {
                                Autodesk.Revit.DB.View vw = null;

                                using (Transaction curTrans = new Transaction(curDoc, "Create Dependent View Transaction"))
                                {
                                    if (curTrans.Start() == TransactionStatus.Started)
                                    {
                                        Autodesk.Revit.DB.View vwParent = curDoc.GetViewByName(cd.parent);

                                        if (vwParent != null)
                                        {
                                            try
                                            {
                                                ElementId eid = vwParent.Duplicate(ViewDuplicateOption.AsDependent);
                                                vw = curDoc.GetElement(eid) as Autodesk.Revit.DB.View;
                                            }
                                            catch (Exception ex)
                                            {
                                                logger.Error("Exception trying tocreate a dependent view!", ex);
                                                continue;
                                            }
                                        }
                                    }

                                    curTrans.Commit();
                                }

                                if (vw != null)
                                    UpdateCrops(" => created and updated sucessfully", curDoc, vw, cd, sb);
                            }
                        }
                    }
                }

                // and in summary...
                rtResults.Text = sb.ToString();
                logger.Info(sb.ToString());
            }

            reasonMessage = string.Empty;
            return true;
        }

        private void UpdateCrops(string msg, Document curDoc, Autodesk.Revit.DB.View vw, CropData cd, StringBuilder sb)
        {
            Transaction transaction = new Transaction(curDoc, "syncing the dependent views");
            if (transaction.Start() == TransactionStatus.Started)
            {
                try
                {
                    var vcrsm = vw.GetCropRegionShapeManager();

                    vcrsm.BottomAnnotationCropOffset = cd.bottomOffset;
                    vcrsm.TopAnnotationCropOffset = cd.topOffset;
                    vcrsm.RightAnnotationCropOffset = cd.rightOffset;
                    vcrsm.LeftAnnotationCropOffset = cd.leftOffset;

                    JsonSerializerSettings jss = new JsonSerializerSettings();
                    jss.Converters.Add(new JsonVector3Converter());
                    jss.Converters.Add(new JsonMyCurveConverter());
                    List<MyCurve> myCurves = JsonConvert.DeserializeObject<List<MyCurve>>(cd.cropRegionJSON, jss);

                    CurveLoop loop = new CurveLoop();
                    foreach (MyCurve curve in myCurves)
                    {
                        Line line = Line.CreateBound((XYZ)curve.Origin, (XYZ)curve.EndPoint);
                        loop.Append(line);
                    }

                    vcrsm.SetCropShape(loop);

                    transaction.Commit();
                    sb.AppendLine("" + cd.name + msg);
                }
                catch (Exception ex)
                {
                    transaction.RollBack();

                    sb.AppendLine("" + cd.name + " => threw an exception (barfed)");
                    logger.Error("Exception occurred", ex);
                }

            }
        }

        private void m_buttonNext_Click(object sender, EventArgs e)
        {
            string reasonMessage = string.Empty;
            if (NextButtonClicked(out reasonMessage))
            {
                int currentIndex = currentPanelIndex;
                currentIndex++;
                if (!(currentIndex >= tabControl.TabCount))
                {
                    SetCurrentTab(currentIndex);
                    m_buttonBack.Visible = true;

                    if (currentIndex == tabControl.TabCount - 1)
                    {
                        // lets disable the back button since there is nothing you can do about it now...
                        m_buttonBack.Visible = false;
                        m_buttonNext.Visible = false;
                        m_buttonNext.SendToBack();
                        m_buttonFinish.Visible = true;
                        m_buttonFinish.Enabled = true;
                    }
                }
            }
            else
            {
                if (reasonMessage.Length > 0)
                    MessageBox.Show(reasonMessage, "Error", MessageBoxButtons.OK);
            }
        }

        private void m_buttonBack_Click(object sender, EventArgs e)
        {
            int currentIndex = currentPanelIndex;
            currentIndex--;
            if (currentIndex >= 0)
            {
                SetCurrentTab(currentIndex);

                m_buttonNext.Visible = true;
                m_buttonFinish.Visible = false;
                m_buttonFinish.SendToBack();

                if (currentIndex == 0)
                    m_buttonBack.Visible = false;
            }
        }

        private void m_buttonFinish_Click(object sender, EventArgs e)
        {
            // show the results of our work...

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void tvCreate_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (e.Action != TreeViewAction.Unknown)
            {
                TreeView tree = sender as TreeView;
                TreeNode node = e.Node;
                TreeViewAction action = e.Action;

                bool nodeChecked = node.Checked;
                bool fParentNode = node.Parent == null;
                if (fParentNode)
                {
                    // check/uncheck all children
                    foreach (TreeNode tn in node.Nodes)
                    {
                        tn.Checked = nodeChecked;
                    }
                }
                else
                {
                    if (nodeChecked)
                    {
                        // select parent
                        node.Parent.Checked = true;
                    }
                    else
                    {
                        bool fAnyChecked = false;
                        // see if parent has any selected children...
                        foreach (TreeNode tn in node.Parent.Nodes)
                        {
                            if (tn.Checked)
                                fAnyChecked = true;
                        }
                        node.Parent.Checked = fAnyChecked;
                    }
                }
            }
        }
    }
}
