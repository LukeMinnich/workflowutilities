﻿using Autodesk.Revit.DB;
using System.Collections.Generic;
using System.Linq;

namespace KaterraUtilities.Extensions
{
    public static class ViewExtensions
    {
        public static bool CreateViewSection(this View vSecFrom, Document doc)
        {
            bool ret = false;

            IEnumerable<ViewFamilyType> viewFamilyTypes = from elem in new FilteredElementCollector(doc).OfClass(typeof(ViewFamilyType))
                                                          let type = elem as ViewFamilyType
                                                          where type.ViewFamily == ViewFamily.Section
                                                          select type;
            ElementId typeId = viewFamilyTypes.First().Id;

            BoundingBoxXYZ sectionBox = new BoundingBoxXYZ();

            BoundingBoxXYZ viewBox = vSecFrom.get_BoundingBox(null);
            sectionBox.Transform = viewBox.Transform;
            XYZ midP = 0.5 * (viewBox.Max + viewBox.Min);
            XYZ midWithOffset = new XYZ(midP.X, midP.Y, midP.Z - 0.2);
            sectionBox.Transform.Origin = new XYZ(0, 0, -1000); // midWithOffset;

            sectionBox.Min = viewBox.Min;
            sectionBox.Max = viewBox.Max; // or CropBox

            ViewSection vSecTo = ViewSection.CreateSection(doc, typeId, sectionBox);
            vSecTo.Scale = vSecFrom.Scale;
            vSecTo.Name = vSecFrom.Name;
            vSecTo.CropBox = new BoundingBoxXYZ();
            vSecTo.CropBox.Transform.Origin = new XYZ(0, 0, -1000);

            if (vSecTo.ParametersMap.Contains("VIEW PRIMARY CATEGORY"))
            {
                Parameter para = vSecFrom.ParametersMap.get_Item("VIEW PRIMARY CATEGORY");
                string value = para.GetParameterInformation(doc);

                Parameter paraTo = vSecTo.ParametersMap.get_Item("VIEW PRIMARY CATEGORY");
                paraTo.SetParameter(doc, value);
            }
            if (vSecTo.ParametersMap.Contains("VIEW SECONDARY CATEGORY"))
            {
                Parameter paraFrom = vSecFrom.ParametersMap.get_Item("VIEW SECONDARY CATEGORY");
                string value = paraFrom.GetParameterInformation(doc);

                Parameter paraTo = vSecTo.ParametersMap.get_Item("VIEW SECONDARY CATEGORY");
                paraTo.SetParameter(doc, value);
            }

            CopyPasteOptions options = new CopyPasteOptions();
            options.SetDuplicateTypeNamesHandler(new HideAndAcceptDuplicateTypeNamesHandler());

            ElementTransformUtils.CopyElements(
                vSecFrom,
                vSecFrom.GetAllViewElements(doc),
                vSecTo,
                sectionBox.Transform,
                options);
            return ret;
        }

        public static ICollection<ElementId> GetAllViewElements(this View view, Document doc)
        {
            List<ElementId> elements = new List<ElementId>();

            FilteredElementCollector collector = new FilteredElementCollector(doc, view.Id).WhereElementIsNotElementType();

            int cnt0 = 0;
            int cnt1 = 0;
            foreach (Element e in collector)
            {
                cnt0++;
                if (null != e.Category)
                {
                    cnt1++;
                    elements.Add(e.Id);
                }
            }

            return elements;
        }
    }

    /// <summary>
    /// A handler to accept duplicate types names created by the copy/paste operation.
    /// </summary>
    class HideAndAcceptDuplicateTypeNamesHandler : IDuplicateTypeNamesHandler
    {
        #region IDuplicateTypeNamesHandler Members

        /// <summary>
        /// Implementation of the IDuplicateTypeNameHandler
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public DuplicateTypeAction OnDuplicateTypeNamesFound(DuplicateTypeNamesHandlerArgs args)
        {
            // Always use duplicate destination types when asked
            return DuplicateTypeAction.UseDestinationTypes;
        }

        #endregion
    }
}
