﻿using Autodesk.Revit.DB;

namespace KaterraUtilities.Extensions
{
    public static class ParameterExtensions
    {
        public static string GetParameterInformation(this Parameter para, Document doc)
        {
            string ret = string.Empty;
            switch (para.StorageType)
            {
                case StorageType.Double:
                    //covert the number into Metric
                    ret += para.AsValueString();
                    break;
                case StorageType.ElementId:
                    //find out the name of the element
                    Autodesk.Revit.DB.ElementId id = para.AsElementId();
                    if (id.IntegerValue >= 0)
                    {
                        ret += doc.GetElement(id).Name;
                    }
                    else
                    {
                        ret += id.IntegerValue.ToString();
                    }
                    break;
                case StorageType.Integer:
                    if (ParameterType.YesNo == para.Definition.ParameterType)
                    {
                        if (para.AsInteger() == 0)
                        {
                            ret += "False";
                        }
                        else
                        {
                            ret += "True";
                        }
                    }
                    else
                    {
                        ret += para.AsInteger().ToString();
                    }
                    break;
                case StorageType.String:
                    ret += para.AsString();
                    break;
                default:
                    ret = "Unexposed parameter.";
                    break;
            }

            return ret;

        }

        public static string GetParameterName(this Parameter para)
        {
            return para.Definition.Name;
        }
        
        public static bool SetParameterValue(this Parameter para, Document doc, string value)
        {
            bool result = false;
            if (!para.IsReadOnly)
            {
                result = para.Set(value);
            }
            return result;
        }

        public static bool SetParameter(this Parameter para, Document doc, string value)
        {
            bool result = false;
            if (!para.IsReadOnly)
            {
                result = para.Set(value);
            }
            return result;
        }
    }
}
