﻿using Autodesk.Revit.DB;
using KaterraUtilities.database.models;
using KaterraUtilities.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Diagnostics;
using RevDB = Autodesk.Revit.DB;

namespace KaterraUtilities.Extensions
{
    public static class DocumentExtensions
    {
        /// <summary>
        /// returns a list of all the views in the current model file
        /// </summary>
        /// <param name="curDoc"></param>
        /// <returns></returns>
        public static List<View> GetAllViews(this Document curDoc)
        {
            FilteredElementCollector colViews = new FilteredElementCollector(curDoc);
            colViews.OfCategory(BuiltInCategory.OST_Views);

            List<View> curViews = new List<View>();
            foreach (View x in colViews)
            {
                if (x.IsTemplate == false)
                {
                    curViews.Add(x);
                }
            }

            return curViews;
        }

        public static View GetViewByNameAndType(this Document curDoc, NameTypePair pair)
        {
            return curDoc.GetViewByNameAndType(pair.Name, pair.Type);
        }

        /// <summary>
        /// return the specified view
        /// </summary>
        /// <param name="curDoc"></param>
        /// <param name="viewTypeName"></param>
        /// <param name="viewName"></param>
        /// <returns></returns>
        public static View GetViewByNameAndType(this Document curDoc, string viewName, string viewTypeName)
        {
            List<View> viewCollector = curDoc.GetAllViews();

            foreach (View tmpView in viewCollector)
            {
                string tmpTypeName = tmpView.ViewType.ToString();

                if (tmpView.Name.Equals(viewName) && tmpTypeName.Equals(viewTypeName))
                {
                    return tmpView;
                }
            }

            return null;
        }

        public static View GetViewByName(this Document curDoc, string viewName)
        {
            List<View> viewCollector = curDoc.GetAllViews();

            foreach (View tmpView in viewCollector)
            {
                string tmpTypeName = tmpView.ViewType.ToString();

                if (tmpView.Name.Equals(viewName))
                {
                    return tmpView;
                }
            }

            return null;
        }

        public static Dictionary<string, List<CropData>> GetDependentViews(this Document curDoc)
        {
            List<RevDB.View> viewList = curDoc.GetAllViews(); 
            Dictionary<string, List<CropData>> depViews = new Dictionary<string, List<CropData>>();

            foreach (RevDB.View curView in viewList)
            {
                var e = curDoc.GetElement(curView.Id);
                ParameterSet pSet = e.Parameters;

                Parameter p = e.ParametersMap.get_Item("Dependency");
                string depString = p.AsString();
                Debug.WriteLine(curView.Name + " dependency: " + p.AsString());

                if (depString.StartsWith("Dependent on "))
                {
                    string parent = depString.Substring("Dependent on ".Length);
                    if (!depViews.ContainsKey(parent))
                    {
                        depViews[parent] = new List<CropData>();
                    }
                    CropData cropData = new CropData();
                    cropData.name = curView.Name;
                    cropData.parent = parent;
                    var vcrsm = curView.GetCropRegionShapeManager();
                    cropData.bottomOffset = vcrsm.BottomAnnotationCropOffset;
                    cropData.topOffset = vcrsm.TopAnnotationCropOffset;
                    cropData.rightOffset = vcrsm.RightAnnotationCropOffset;
                    cropData.leftOffset = vcrsm.LeftAnnotationCropOffset;
                    IList<CurveLoop> shapes = vcrsm.GetCropShape();
                    IEnumerator<Curve> curves = shapes[0].GetEnumerator();

                    List<MyCurve> myCurves = new List<MyCurve>();
                    while (curves.MoveNext())
                    {
                        MyCurve mc = new MyCurve();
                        Curve curve = curves.Current;
                        if (curve is Line)
                        {
                            mc.Origin = new Vector3(curve.GetEndPoint(0)); // this is origin
                            mc.EndPoint = new Vector3(curve.GetEndPoint(1));
                        }

                        myCurves.Add(mc);
                    }
                    var json = JsonConvert.SerializeObject(myCurves);
                    List<MyCurve> copyOfMyCurves = JsonConvert.DeserializeObject<List<MyCurve>>(json);

                    cropData.cropRegionJSON = json;
                    depViews[parent].Add(cropData);
                }
            }

            return depViews;
        }
    }
}
