﻿using Autodesk.Revit.DB;
using KaterraUtilities.Extensions;
using KaterraUtilities.Models;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using RevDB = Autodesk.Revit.DB;

namespace KaterraUtilities.DupViews
{
    public partial class DupViewForm : System.Windows.Forms.Form
    {
        private void DupViewForm_Load(object sender, EventArgs e)
        {

        }

        public DupViewForm(Document curDoc)
        {
            // The Me.InitializeComponent call is required for Windows Forms designer support.
            InitializeComponent();

            //set selected duplicate option
            rbDup.Checked = true;

            FillTreeWithViews(curDoc);
            ExpandNode("DraftingView");
        }

        private void FillTreeWithViews(Document curDoc)
        {
            //get all views in current model file
            List<RevDB.View> viewList = null;
            viewList = curDoc.GetAllViews();

            // treeOfViewTypes
            foreach (RevDB.View curView in viewList)
            {
                string viewTypeName = curView.ViewType.ToString();

                // check to see if this type is in the tree as a root, and if not, create the root, then add the view.
                var idxRoot = treeOfViewTypes.Nodes.IndexOfKey(viewTypeName);
                if (idxRoot == -1)
                {
                    var temp = new TreeNode(viewTypeName, 1, 1);
                    temp.Name = viewTypeName;
                    treeOfViewTypes.Nodes.Add(temp);
                }

                idxRoot = treeOfViewTypes.Nodes.IndexOfKey(viewTypeName); // won't be -1 now
                var tempChild = new TreeNode(curView.Name, 1, 1);
                tempChild.Name = curView.Name;
                treeOfViewTypes.Nodes[idxRoot].Nodes.Add(tempChild);
            }
        }

        private void ExpandNode(string nodeText)
        {
            // expand the drafting views since that was the original purpose of this work
            foreach (TreeNode parent in treeOfViewTypes.Nodes)
            {
                string strType = parent.Name;
                if (strType.Equals(nodeText))
                {
                    parent.Expand();
                }
            }
        }

        public List<NameTypePair> GetSelectedViews()
        {
            List<NameTypePair> viewList = new List<NameTypePair>();

            foreach (TreeNode parent in treeOfViewTypes.Nodes)
            {
                string strType = parent.Name;
                foreach (TreeNode child in parent.Nodes)
                {
                    if (child.Checked)
                    {
                        string strName = child.Name;
                        viewList.Add(new NameTypePair() { Name = strName, Type = strType });
                    }
                }
            }

            return viewList;
        }

        public ViewDuplicateOption GetDuplicateType()
        {
            //return duplicate type value
            if (this.rbDupDetailing.Checked == true)
            {
                return ViewDuplicateOption.WithDetailing;
            }
            else if (this.rbDupDependent.Checked == true)
            {
                return ViewDuplicateOption.AsDependent;
            }
            else
            {
                return ViewDuplicateOption.Duplicate;
            }
        }

        public ViewConvertOption GetConvertType()
        {
            if (this.radioToSection.Checked == true)
            {
                return ViewConvertOption.Section;
            }
            else
            {
                return ViewConvertOption.None;
            }
        }

        // Updates all child tree nodes recursively.
        private void CheckAllChildNodes(TreeNode treeNode, bool nodeChecked)
        {
            foreach (TreeNode node in treeNode.Nodes)
            {
                node.Checked = nodeChecked;
                if (node.Nodes.Count > 0)
                {
                    // If the current node has child nodes, call the CheckAllChildsNodes method recursively.
                    this.CheckAllChildNodes(node, nodeChecked);
                }
            }
        }

        private void treeOfViewTypes_AfterCheck(object sender, TreeViewEventArgs e)
        {
            // The code only executes if the user caused the checked state to change.
            if (e.Action != TreeViewAction.Unknown)
            {
                if (e.Node.Nodes.Count > 0)
                {
                    /* Calls the CheckAllChildNodes method, passing in the current 
                    Checked value of the TreeNode whose checked state changed. */
                    this.CheckAllChildNodes(e.Node, e.Node.Checked);
                }
            }
        }
    }
}
