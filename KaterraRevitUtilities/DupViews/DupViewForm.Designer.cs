﻿namespace KaterraUtilities.DupViews
{
    partial class DupViewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.rbDup = new System.Windows.Forms.RadioButton();
            this.rbDupDetailing = new System.Windows.Forms.RadioButton();
            this.rbDupDependent = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.treeOfViewTypes = new System.Windows.Forms.TreeView();
            this.grpConvert = new System.Windows.Forms.GroupBox();
            this.radioNope = new System.Windows.Forms.RadioButton();
            this.radioToSection = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.grpConvert.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(178, 23);
            this.label2.TabIndex = 11;
            this.label2.Text = "Select Views to Duplicate:";
            this.label2.UseCompatibleTextRendering = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(344, 761);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseCompatibleTextRendering = true;
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(263, 761);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 9;
            this.btnOK.Text = "OK";
            this.btnOK.UseCompatibleTextRendering = true;
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // rbDup
            // 
            this.rbDup.Location = new System.Drawing.Point(6, 19);
            this.rbDup.Name = "rbDup";
            this.rbDup.Size = new System.Drawing.Size(104, 24);
            this.rbDup.TabIndex = 15;
            this.rbDup.TabStop = true;
            this.rbDup.Text = "Duplicate";
            this.rbDup.UseCompatibleTextRendering = true;
            this.rbDup.UseVisualStyleBackColor = true;
            // 
            // rbDupDetailing
            // 
            this.rbDupDetailing.Location = new System.Drawing.Point(6, 49);
            this.rbDupDetailing.Name = "rbDupDetailing";
            this.rbDupDetailing.Size = new System.Drawing.Size(188, 24);
            this.rbDupDetailing.TabIndex = 16;
            this.rbDupDetailing.TabStop = true;
            this.rbDupDetailing.Text = "Duplicate with Detailing";
            this.rbDupDetailing.UseCompatibleTextRendering = true;
            this.rbDupDetailing.UseVisualStyleBackColor = true;
            // 
            // rbDupDependent
            // 
            this.rbDupDependent.Location = new System.Drawing.Point(6, 79);
            this.rbDupDependent.Name = "rbDupDependent";
            this.rbDupDependent.Size = new System.Drawing.Size(217, 24);
            this.rbDupDependent.TabIndex = 17;
            this.rbDupDependent.TabStop = true;
            this.rbDupDependent.Text = "Duplicate as a Dependent";
            this.rbDupDependent.UseCompatibleTextRendering = true;
            this.rbDupDependent.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.rbDup);
            this.groupBox1.Controls.Add(this.rbDupDependent);
            this.groupBox1.Controls.Add(this.rbDupDetailing);
            this.groupBox1.Location = new System.Drawing.Point(222, 644);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(197, 111);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Duplicate Settings";
            this.groupBox1.UseCompatibleTextRendering = true;
            // 
            // treeOfViewTypes
            // 
            this.treeOfViewTypes.CheckBoxes = true;
            this.treeOfViewTypes.Location = new System.Drawing.Point(13, 35);
            this.treeOfViewTypes.Name = "treeOfViewTypes";
            this.treeOfViewTypes.Size = new System.Drawing.Size(404, 603);
            this.treeOfViewTypes.TabIndex = 20;
            this.treeOfViewTypes.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.treeOfViewTypes_AfterCheck);
            // 
            // grpConvert
            // 
            this.grpConvert.Controls.Add(this.radioToSection);
            this.grpConvert.Controls.Add(this.radioNope);
            this.grpConvert.Location = new System.Drawing.Point(12, 644);
            this.grpConvert.Name = "grpConvert";
            this.grpConvert.Size = new System.Drawing.Size(204, 71);
            this.grpConvert.TabIndex = 22;
            this.grpConvert.TabStop = false;
            this.grpConvert.Text = "Convert";
            // 
            // radioNope
            // 
            this.radioNope.AutoSize = true;
            this.radioNope.Location = new System.Drawing.Point(7, 20);
            this.radioNope.Name = "radioNope";
            this.radioNope.Size = new System.Drawing.Size(74, 17);
            this.radioNope.TabIndex = 0;
            this.radioNope.TabStop = true;
            this.radioNope.Text = "No thanks";
            this.radioNope.UseVisualStyleBackColor = true;
            // 
            // radioToSection
            // 
            this.radioToSection.AutoSize = true;
            this.radioToSection.Checked = true;
            this.radioToSection.Location = new System.Drawing.Point(7, 44);
            this.radioToSection.Name = "radioToSection";
            this.radioToSection.Size = new System.Drawing.Size(77, 17);
            this.radioToSection.TabIndex = 1;
            this.radioToSection.TabStop = true;
            this.radioToSection.Text = "To Section";
            this.radioToSection.UseVisualStyleBackColor = true;
            // 
            // DupViewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(429, 792);
            this.Controls.Add(this.grpConvert);
            this.Controls.Add(this.treeOfViewTypes);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Name = "DupViewForm";
            this.Text = "Batch Duplicate Views";
            this.Load += new System.EventHandler(this.DupViewForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.grpConvert.ResumeLayout(false);
            this.grpConvert.PerformLayout();
            this.ResumeLayout(false);

        }
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbDupDependent;
        private System.Windows.Forms.RadioButton rbDupDetailing;
        private System.Windows.Forms.RadioButton rbDup;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label2;
        #endregion

        private System.Windows.Forms.TreeView treeOfViewTypes;
        private System.Windows.Forms.GroupBox grpConvert;
        private System.Windows.Forms.RadioButton radioToSection;
        private System.Windows.Forms.RadioButton radioNope;
    }
}