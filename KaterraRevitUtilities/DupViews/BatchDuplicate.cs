﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using KaterraUtilities.Extensions;
using KaterraUtilities.Models;
using log4net;
using System;
using System.Collections.Generic;

namespace KaterraUtilities.DupViews
{
    /// <summary>
    /// Batch up a bunch of views and dupicate them.
    /// </summary>
    public static class BatchDuplicate
    {
        private static ILog logger = LogManager.GetLogger(typeof(BatchDuplicate));
        private static int counter = 0;

        /// <summary>
        /// Duplicate a group of views
        /// </summary>
        /// <param name="curDoc"></param>
        public static void BatchDuplicateViews(Document curDoc)
        {
            counter = 0;

            using (DupViewForm curForm = new DupViewForm(curDoc))
            {
                curForm.ShowDialog();

                if (curForm.DialogResult == System.Windows.Forms.DialogResult.Cancel)
                {
                    return;
                }
                else
                {
                    if (curForm.GetSelectedViews().Count > 0)
                    {
                        List<NameTypePair> viewList = curForm.GetSelectedViews();
                        ViewDuplicateOption dupOptions = curForm.GetDuplicateType();
                        ViewConvertOption convertOption = curForm.GetConvertType();

                        BatchProcess(curDoc, viewList, dupOptions, convertOption);

                        TaskDialog.Show("Complete", "Created " + counter + " view duplicates.");
                    }
                    else
                    {
                        TaskDialog.Show("Error", "Please select views to duplicate.");
                        return;
                    }
                }
            }
        }

        private static void BatchProcess(Document curDoc, List<NameTypePair> viewList, ViewDuplicateOption dupOptions, ViewConvertOption convertOption)
        {
            using (Transaction curTrans = new Transaction(curDoc, "Current Transaction"))
            {
                if (curTrans.Start() == TransactionStatus.Started)
                {
                    foreach (NameTypePair pair in viewList)
                    {
                        View curView = curDoc.GetViewByNameAndType(pair.Name, pair.Type);

                        if (curView != null)
                        {
                            try
                            {
                                switch (convertOption)
                                {
                                    case ViewConvertOption.None:
                                        ElementId eid = curView.Duplicate(dupOptions);
                                        break;

                                    case ViewConvertOption.Section:
                                        curView.CreateViewSection(curDoc);
                                        break;
                                }
                                counter++;
                            }
                            catch (Exception ex)
                            {
                                TaskDialog.Show("Error", "Could not duplicate view.");
                                logger.Error("Exceptionm trying to save batch files.", ex);
                                throw;
                            }
                        }
                    }
                }

                curTrans.Commit();
            }
        }
    }
}
