﻿using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Macros;
using Autodesk.Revit.UI;
using KaterraUtilities.DupViews;

namespace KaterraUtilities.Commands
{
    [Transaction(TransactionMode.Manual)]
    [AddInId("735E8164-F2C2-4B24-980B-37212F1ED5F5")]
    class CopyPasteDraftingView2Section : IExternalCommand
    {
        public Autodesk.Revit.UI.Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            UIApplication uiApp = commandData.Application;
            UIDocument uiDoc = uiApp.ActiveUIDocument;

            //run batch duplicate
            BatchDuplicate.BatchDuplicateViews(uiDoc.Document);

            return Autodesk.Revit.UI.Result.Succeeded;
        }
    }
}
