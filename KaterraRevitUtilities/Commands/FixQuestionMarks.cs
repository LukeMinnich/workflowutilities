﻿using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using System.Collections.Generic;

namespace KaterraUtilities
{
    /// <remarks>
    /// The "FixQuestionMarks" external command. The class must be Public.
    /// </remarks>
    [Transaction(TransactionMode.Manual)]

    public class FixQuestionMarks : IExternalCommand
    {
        // The main Execute method (inherited from IExternalCommand) must be public
        public Autodesk.Revit.UI.Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            UIApplication uiApp = commandData.Application;
            UIDocument uiDoc = uiApp.ActiveUIDocument;

            MaterialTagFix(uiDoc);

            return Autodesk.Revit.UI.Result.Succeeded;
        }

        private void MaterialTagFix(UIDocument uiDoc)
        {
            uiDoc.RefreshActiveView(); //Refresh the view - Unnecessary
            List<ElementId> noIDs = new List<ElementId>();
            List<Element> tags = new List<Element>();

            List<Element> mtags = new List<Element>();
            List<Element> pinnedtags = new List<Element>();
            noIDs.Add(ElementId.InvalidElementId); //create a null selection for later use

            List<ElementId> selectedIds = new List<ElementId>(); // a list for selected elements

            foreach (Element e in new FilteredElementCollector(uiDoc.Document).OfCategory(BuiltInCategory.OST_MaterialTags).OfClass(typeof(IndependentTag))) //collect material tags in the document
            {
                tags.Add(e); //store in list of tags
                selectedIds.Add(e.Id); //store the tag ID in the list for selecting items
            }

            List<ElementId> views2open = new List<ElementId>(); // a list for views to be opened


            foreach (Element tag in tags) //find the containing view for all material tags
            {
                IndependentTag itag = tag as IndependentTag;
                if (itag != null)
                {
                    ElementId vid = itag.OwnerViewId;
                    if (!views2open.Contains(vid))
                    {
                        views2open.Add(vid); //add the view to the list only if does not exist 
                    }
                }
            }

            View startingView = uiDoc.ActiveView; //store the starting view as a courtesy to the user

            foreach (ElementId v2oId in views2open) //open all views containing material tags
            {
                View v2o = uiDoc.Document.GetElement(v2oId) as View;
                if (v2o != null)
                {
                    uiDoc.ActiveView = v2o; //get the view ID and set it as active

                    UIView uiview = null;
                    IList<UIView> uiviews = uiDoc.GetOpenUIViews();

                    foreach (UIView uv in uiviews) //get the corresponding UIview to the opened view
                    {
                        if (uv.ViewId.Equals(v2oId))
                        {
                            uiview = uv;
                            break;
                        }
                    }

                    if (null != uiview) //if corresponding UIview exists, set the UIview zoom to the view's crop
                    {
                        BoundingBoxXYZ crop = v2o.CropBox;
                        XYZ p = crop.Min;
                        XYZ q = crop.Max;
                        uiview.ZoomAndCenterRectangle(p, q);
                    }

                    uiDoc.Selection.SetElementIds(selectedIds); //select all material tags
                    uiDoc.RefreshActiveView(); // refresh the view
                    uiDoc.Selection.SetElementIds(noIDs); // clear the selection
                }
            }
            
            uiDoc.ActiveView = startingView; // set the view back to the starting view
            uiDoc.Selection.SetElementIds(selectedIds); // select and refresh, because why not?
            uiDoc.RefreshActiveView();
            uiDoc.Selection.SetElementIds(noIDs);

            if (views2open.Contains(startingView.Id)) // if the starting view contained tags
            {
                views2open.Remove(startingView.Id); // remove it from the list as we want to close all of them, just a courtesy to the user
            }
            bool closeviews = true; // set to false in case you don't want to close the views
            if (closeviews)
            {
                foreach (UIView openview in uiDoc.GetOpenUIViews())
                {
                    if (views2open.Contains(openview.ViewId))
                    {
                        openview.Close(); // close the views that were opened by the macro
                    }
                }
                uiDoc.ActiveView = startingView; // set the the view back to the starting view - redundant
            }
        }

    }
}
