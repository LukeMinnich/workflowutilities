﻿using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace KaterraUtilities
{
    /// <remarks>
    /// The "SaveGroups" external command. The class must be Public.
    /// </remarks>
    [Transaction(TransactionMode.Manual)]
    public class SaveGroups : IExternalCommand
    {
        // The main Execute method (inherited from IExternalCommand) must be public
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            Document doc = commandData.Application.ActiveUIDocument.Document;
            string executablePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            
            // start up the remote controller with the "SaveModels" action passed in
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = executablePath + "/RemoteControl/RevitRemoteControl.exe";
            startInfo.Arguments = "SaveModels";
            Process.Start(startInfo);

            // and then just return and let the remote do its job!
            return Result.Succeeded;
        }
    }
}

/* potentially useful code snippet for the future...
 * 
            StringBuilder b = new StringBuilder();

            FilteredElementCollector collector = new FilteredElementCollector(doc).OfClass((typeof(Group)));
            List<Element> list = collector.OrderBy(x => x.Name).ToList();

            int i = 0;
            foreach (Element e in list)
            {
                Group group = e as Group;
                if (null != group)
                {
                    if (i++ == 1)
                    {
                        // can i select the group here?
                        RevitCommandId commandId = RevitCommandId.LookupPostableCommandId(PostableCommand.SaveAsLibraryGroup);

                        if (commandData.Application.CanPostCommand(commandId))
                        {
                            commandData.Application.PostCommand(commandId);
                            //commandData.Application.ActiveUIDocument.Document.
                        }                        
                    }
                    b.AppendLine(group.GroupType.FamilyName + ": " + group.Name);
                }
            }
*/