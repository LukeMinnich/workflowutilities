﻿using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;

namespace KaterraUtilities
{
    /// <remarks>
    /// The "HelloWorld" external command. The class must be Public.
    /// </remarks>
    [Transaction(TransactionMode.Manual)]
    public class HelloWorld : IExternalCommand
    {
        // The main Execute method (inherited from IExternalCommand) must be public
        public Autodesk.Revit.UI.Result Execute(ExternalCommandData revit, ref string message, ElementSet elements)
        {
            System.Diagnostics.Process.Start("https://katerra.atlassian.net/wiki/spaces/KW/pages/375193636/Katerra+Add-Ins");
            return Autodesk.Revit.UI.Result.Succeeded;
        }
    }
}
