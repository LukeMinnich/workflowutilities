﻿using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using KaterraUtilities.Dependents;
using System.IO;
using System.Reflection;

namespace KaterraUtilities.Commands
{
    [Transaction(TransactionMode.Manual)]
    public class SaveDependentInfo : IExternalCommand
    {
        // The main Execute method (inherited from IExternalCommand) must be public
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            UIApplication uiApp = commandData.Application;
            UIDocument uiDoc = uiApp.ActiveUIDocument;

            //run batch duplicate
            BatchDependentActions.BatchDuplicateViews(uiDoc.Document, true);

            return Result.Succeeded;
        }
    }
}
