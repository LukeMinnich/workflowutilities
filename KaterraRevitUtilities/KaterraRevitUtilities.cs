﻿using Autodesk.Revit.UI;
using KaterraUtilities.MenusNRibbons;
using log4net;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace KaterraUtilities
{
    public class KaterraRevitUtilities : IExternalApplication
    {
        private ILog logger = LogManager.GetLogger(typeof(KaterraRevitUtilities));

        public static UIControlledApplication _application;

        public static string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        public Result OnStartup(UIControlledApplication application)
        {
            // since we are in a dll, we need to programatically load the config file for log4net
            if (!log4net.LogManager.GetRepository().Configured)
            {
                var configFile = new FileInfo(AssemblyDirectory + "\\log4net.config");

                if (!configFile.Exists)
                {
                    throw new FileLoadException(String.Format("The configuration file {0} does not exist", configFile));
                }

                log4net.Config.XmlConfigurator.Configure(configFile);
            }

            logger.Info("OnStartup called");
            Debug.WriteLine("OnStartup called");

            _application = application;

            // Create a custom ribbon tab with buttons and stuff just for Katerra
            String tabName = "Katerra";
            application.CreateRibbonTab(tabName);

            logger.Info("OnStartup - CreateRibbonTab called");
            Debug.WriteLine("OnStartup - CreateRibbonTab called");

            RevitMenuBar bar = new RevitMenuBar();
            bar.AddRibbon(_application, new TabPanelData()
                {
                    TabName = tabName,
                    PanelName = "Utilities"
                })
                    .AddButton(new AddExtraButtonData()
                    {
                        ID = "cmdSaveGroups",
                        Text = "Save Groups",
                        ClassName = "KaterraUtilities.SaveGroups",
                        ToolTip = "Save all of the groups to a directory of the useers choice.",
                        ImageName = "Diagram"
                    })
                    .AddButton(new AddExtraButtonData()
                    {
                        ID = "cmdBatchCopyPaste",
                        Text = "Copy/Paste",
                        ClassName = "KaterraUtilities.Commands.CopyPasteDraftingView2Section",
                        ToolTip = "Batch copy all of the Drafting Views to Sections",
                        ImageName = "Copy"
                    })
                .Done()
                .AddRibbon(_application, new TabPanelData()
                {
                    TabName = tabName,
                    PanelName = "Fixups"
                })
                    //.AddButton("cmdBatchCopyPaste", "Batch Copy \nand Paste", "KaterraUtilities.Commands.CopyPasteDraftingView2Section", "Batch copy all of the Drafting Views to Sections", "BandAid")
                    //.AddButtonGroup("Fixups", "Fixups", "QuestionMarks")
                        //    .AddButton("cmdHelloWorld2", "Hello World", "KaterraUtilities.HelloWorld", "Say hello to the entire world.", "Diagram")
                        .AddButton(new AddExtraButtonData()
                        {
                            ID = "cmdJustTesting2",
                            Text = "Fix the \nQuestion Marks",
                            ClassName = "KaterraUtilities.FixQuestionMarks",
                            ToolTip = "Say hello to the entire world 2.",
                            ImageName = "QuestionMarks"
                        })
                .Done()
                .AddRibbon(_application, new TabPanelData()
                {
                    TabName = tabName,
                    PanelName = "Dependent"
                })
                    .AddButtonGroup(new AddButtonModel()
                    {
                        ID = "Dependent",
                        ImageName = "Dependent",
                        Text = "Dependent"
                    })
                        .AddButton(new AddExtraButtonData()
                        {
                            ID = "cmdSaveDependentInfo",
                            Text = "Save Master Dependent Info",
                            ClassName = "KaterraUtilities.Commands.SaveDependentInfo",
                            ToolTip = "Save all of the clip data for all dependent views",
                            ImageName = "SaveDependentInfo"
                        })
                        .AddButton(new AddExtraButtonData()
                        {
                            ID = "cmdSyncDependentInfo",
                            Text = "Sync Linked Dependent Info",
                            ClassName = "KaterraUtilities.Commands.SyncDependentInfo",
                            ToolTip = "Sync all of the clip data for dependent views",
                            ImageName = "SyncDependentInfo"
                        })
                    .Done()
                .Done()
                .AddRibbon(_application, new TabPanelData()
                {
                    TabName = tabName,
                    PanelName = "Help"
                })
                    .AddButton(new AddExtraButtonData()
                    {
                        ID = "cmdHelp",
                        Text = "Help on Katerra Ribbon Usage",
                        ClassName = "KaterraUtilities.HelloWorld",
                        ToolTip = "Launch a browser at the help page",
                        ImageName = "BandAid"
                    })
                    ;

            logger.Info("OnStartup - RevitMenuBar maybe created" + bar.ToString());
            Debug.WriteLine("OnStartup - RevitMenuBar maybe created, bar = " + bar.ToString());

            return Result.Succeeded;
        }

        public Result OnShutdown(UIControlledApplication application)
        {
            // nothing to clean up in this simple case
            return Result.Succeeded;
        }
    }
}
