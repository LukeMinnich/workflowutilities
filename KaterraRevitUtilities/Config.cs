﻿using System.Collections.Generic;
using System.Configuration;

namespace KaterraUtilities
{
    public sealed class Config
    {
        private static Config instance = null;
        private static readonly object padlock = new object();

        private Dictionary<string, string> _configData = new Dictionary<string, string>();

        public static Config I
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new Config();
                    }
                    return instance;
                }
            }
        }

        public Config()
        {
            Configuration myDllConfig = ConfigurationManager.OpenExeConfiguration(this.GetType().Assembly.Location);
            AppSettingsSection myDllConfigAppSettings = (AppSettingsSection)myDllConfig.GetSection("appSettings");

            // get all of the fields into a map for latter use!
            var coll = myDllConfigAppSettings.Settings;//.Settings["MyAppSettingsKey"].Value;
            foreach (var key in myDllConfigAppSettings.Settings.AllKeys)
            {
                string value = myDllConfigAppSettings.Settings[key].Value;
                _configData[key] = value;
            }
        }

        public string this[string key]
        {
            get
            {
                // will throw on purpose if does not exist
                return _configData[key];
            }
        }
    }
}
