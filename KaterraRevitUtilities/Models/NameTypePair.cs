﻿namespace KaterraUtilities.Models
{
    /// <summary>
    /// Simple holder for Name and Type pairings
    /// </summary>
    public class NameTypePair
    {
        /// <summary>The Name of the object</summary>
        public string Name { get; set; }

        /// <summary>The Type of the object</summary>
        public string Type { get; set; }

        public override string ToString()
        {
            return string.Format($"{Name}: {Type}");
        }
    }
}
