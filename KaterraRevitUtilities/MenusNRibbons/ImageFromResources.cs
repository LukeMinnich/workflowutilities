﻿using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Media.Imaging;

namespace KaterraUtilities.MenusNRibbons
{
    public class ImageFromResources
    {
        public BitmapImage anImage { get; set; }

        /// <summary>
        /// Converts a resource to an ImageBitmap
        /// </summary>
        /// <param name="imageName"></param>
        public ImageFromResources(string imageName)
        {
            System.Resources.ResourceManager rm = new System.Resources.ResourceManager(
                "KaterraUtilities.Images.Resource1", 
                typeof(KaterraRevitUtilities).Assembly
            );
            object objImage = rm.GetObject(imageName);
            anImage = ToBitmapImage((Bitmap)objImage); 
        }

        /// <summary>
        /// implicit operator conversion
        /// </summary>
        /// <param name="ifr"></param>
        public static implicit operator BitmapImage(ImageFromResources ifr)
        {
            return ifr.anImage;
        }

        private BitmapImage ToBitmapImage(Bitmap bitmap)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                bitmap.Save(stream, ImageFormat.Png);

                stream.Position = 0;
                BitmapImage result = new BitmapImage();
                result.BeginInit();
                result.CacheOption = BitmapCacheOption.OnLoad;
                result.StreamSource = stream;
                result.EndInit();
                result.Freeze();
                return result;
            }
        }
    }
}
