﻿using Autodesk.Revit.UI;
using System.Diagnostics;
using System.Reflection;

namespace KaterraUtilities.MenusNRibbons
{
    public class Glob
    {
        public static string thisAssemblyPath = Assembly.GetExecutingAssembly().Location;
    }

    /*
    I want to do this for menu creation clarity:

        new OuterRibbonHolder()
            .AddRibbon()
                .AddButton()
                .AddButton()
                .Done() // returns the parent of the last Button which is the ribbon
            .Done() // returns the parent of the ribbon which is the holder
            .AddRibbon()
                .AddButton()
                .AddButtonGroup()
                    .AddButton()
                    .AddButton()
                .Done()
                .AddButton();

     */

    public class TabPanelData
    {
        public string TabName { get; set; }
        public string PanelName { get; set; }
    }

    public class AddButtonModel
    {
        public string ID { get; set; }
        public string Text { get; set; }
        public string ImageName { get; set; }
    }

    public class AddExtraButtonData : AddButtonModel
    {
        public string ClassName { get; set; }
        public string ToolTip { get; set; }
    }

    public class ButtonGroupHolder
    {
        private RibbonHolder parent;
        private PulldownButton pulldownButton;

        public ButtonGroupHolder(RibbonHolder parent, RibbonPanel ribbonPanel, AddButtonModel data)
        {
            this.parent = parent;

            ImageFromResources img = new ImageFromResources(data.ImageName);
            PulldownButtonData pdbGroup = new PulldownButtonData(data.ID, data.Text);
            pulldownButton = ribbonPanel.AddItem(pdbGroup) as PulldownButton;
            pulldownButton.LargeImage = img;
        }

        public ButtonGroupHolder AddButton(AddExtraButtonData data)
        {
            ImageFromResources img = new ImageFromResources(data.ImageName);
            PushButtonData pdbData = new PushButtonData(data.ID, data.Text, Glob.thisAssemblyPath, data.ClassName);
            PushButton item11 = pulldownButton.AddPushButton(pdbData) as PushButton;
            item11.ToolTip = pdbData.Text; 
            item11.LargeImage = img;

            return this;
        }

        public RibbonHolder Done()
        {
            return parent;
        }
    }

    public class RibbonHolder
    {
        private RevitMenuBar parent;
        private RibbonPanel ribbonPanel;

        public RibbonHolder(RevitMenuBar parent, UIControlledApplication application, TabPanelData data)
        {
            ribbonPanel = application.CreateRibbonPanel(data.TabName, data.PanelName);

            this.parent = parent;
        }

        public RibbonHolder AddButton(AddExtraButtonData data)
        {
            ImageFromResources img = new ImageFromResources(data.ImageName);
            PushButtonData buttonData = new PushButtonData(data.ID, data.Text, Glob.thisAssemblyPath, data.ClassName);
            PushButton pushButton = ribbonPanel.AddItem(buttonData) as PushButton;
            pushButton.ToolTip = data.ToolTip;
            pushButton.LargeImage = img;

            return this;
        }

        public ButtonGroupHolder AddButtonGroup(AddButtonModel data)
        {
            return new ButtonGroupHolder(this, ribbonPanel, data);
        }

        public RevitMenuBar Done()
        {
            return parent;
        }
    }

    public class RevitMenuBar
    {
        public RibbonHolder AddRibbon(UIControlledApplication application, TabPanelData data)
        {
            return new RibbonHolder(this, application, data);
        }
    }
}
