﻿namespace KaterraUtilities.Tests.Interfaces
{
    public interface IParameterHelper
    {
        string GetParameterValueAsString(string parameterName);

        bool HasParameter(string parameterName);

        bool SetParameterValue(string parameterName, string value);
    }
}
