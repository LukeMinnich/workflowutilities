﻿using Autodesk.Revit.DB;
using log4net;
using System;

namespace KaterraUtilities.Tests.Interfaces
{
    public class ParameterHelper : IParameterHelper
    {
        private ILog logger = LogManager.GetLogger(typeof(ParameterHelper));

        private readonly Document _doc;

        public ParameterHelper(Document doc)
        {
            _doc = doc;
        }

        public string GetParameterValueAsString(string parameterName)
        {
            Parameter theParam = GetParameterFromProjectInfo(parameterName);
            if (null == theParam)
            {
                return null;
            }

            // Reads data in the parameter
            return theParam.AsString();
        }

        public bool HasParameter(string parameterName)
        {
            Parameter theParam = GetParameterFromProjectInfo(parameterName);
            if (null == theParam)
            {
                return false;
            }

            return true;
        }

        public bool SetParameterValue(string parameterName, string value)
        {
            if (!HasParameter(parameterName)) { return false; }

            Parameter theParam = GetParameterFromProjectInfo(parameterName);

            Transaction transaction = new Transaction(_doc, "updating parameter " + parameterName);
            if (transaction.Start() == TransactionStatus.Started)
            {
                try
                {
                    theParam.Set(value);
                    transaction.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    transaction.RollBack();
                    logger.Error("Exception occurred", ex);
                }
            }
            return false;

        }

        /// <summary>
        /// Gets the revit parameter from project info
        /// </summary>
        /// <param name="parameterName">name of the parameter</param>
        /// <returns>parameter or null if not found</returns>
        public Parameter GetParameterFromProjectInfo(string parameterName)
        {
            Element pInfo = _doc.ProjectInformation;
            if (null == pInfo)
            {
                return null;
            }
            Parameter theParam = pInfo.LookupParameter(parameterName);
            if (null == theParam)
            {
                return null;
            }

            return theParam;
        }
    }
}
