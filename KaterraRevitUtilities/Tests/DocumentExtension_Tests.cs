﻿using Autodesk.Revit.DB;
using KaterraUtilities.Extensions;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KaterraUtilities.Tests
{
    [TestFixture]
    public class DocumentExtension_Tests
    {
        [SetUp]
        public void Setup()
        {
            // lets make sure we only run these tests on TestArch.rvt
            Document document = Helpers.GeneralHelper.ActiveUIDocument.Document;
            string name = document.Title;

            if (!name.Equals("TestArch.rvt"))
                Assert.Inconclusive("You must load TestArch.rvt to run these tests!");
        }

        [Test]
        public void GetAllViews_Sanity()
        {
            Document document = Helpers.GeneralHelper.ActiveUIDocument.Document;
            List<View> views = document.GetAllViews();
            StringBuilder str = new StringBuilder();

            // uncomment this to get a list of views and thier names/types.
            //foreach(View v in views)
            //{
            //    str.AppendLine(v.Name + v.ViewType.ToString());
            //}
            //Assert.That(false, str.ToString());

            Assert.That(views.Count > 0, "There should be at least 1 view in the list!");
        }

        [Test]
        public void GetAllViews_BadDoc()
        {
            Document document = null;
            Assert.Throws<Autodesk.Revit.Exceptions.ArgumentNullException>(() => document.GetAllViews());
        }

        [Test]
        public void GetViewName_Sanity()
        {
            Document document = Helpers.GeneralHelper.ActiveUIDocument.Document;
            List<View> views = document.GetAllViews();

            // keep this around for debugging
            // Assert.That(false, string.Join(",", views.Select(x => new string(x.Name + ", " + x.ViewType))));

            Assert.That(views.Count > 0, "There should be at least 1 view in the list!");

            View view = document.GetViewByNameAndType("Level 1", "CeilingPlan");
            Assert.That(view != null, "a view was not returned when it should have been.");
        }

        [Test]
        public void GetViewName_Failure()
        {
            Document document = Helpers.GeneralHelper.ActiveUIDocument.Document;
            List<View> views = document.GetAllViews();

            Assert.That(views.Count > 0, "There should be at least 1 view in the list!");

            View view = document.GetViewByNameAndType("This is not a real name!", "DraftingView");
            Assert.That(view == null, "The view should be null in this case.");
        }
    }
}
