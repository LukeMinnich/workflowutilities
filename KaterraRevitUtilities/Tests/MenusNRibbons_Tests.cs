﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using KaterraUtilities.MenusNRibbons;
using NUnit.Framework;

namespace KaterraUtilities.Tests
{
    /// <summary>
    /// Unfortunately, new ribbons and menus really need to be done at startup and not on 
    /// the fly, so there is little we can test here.
    /// </summary>
    [TestFixture]
    public class MenusNRibbons_Tests
    {
        [SetUp]
        public void Setup()
        {
            // lets make sure we only run these tests on TestArch.rvt
            Document document = Helpers.GeneralHelper.ActiveUIDocument.Document;
            string name = document.Title;

            if (!name.Equals("TestArch.rvt"))
                Assert.Inconclusive("You must load TestArch.rvt to run these tests!");
        }

        [Test]
        public void Sanity_MenuBar_Test()
        {
            Assert.DoesNotThrow(() => new RevitMenuBar());
        }
    }
}
