﻿using Autodesk.Revit.DB;
using KaterraUtilities.Extensions;
using KaterraUtilities.Tests.Interfaces;
using NUnit.Framework;

namespace KaterraUtilities.Tests
{
    [TestFixture]
    public class ParameterExtension_Tests
    {
        [SetUp]
        public void Setup()
        {
            // lets make sure we only run these tests on TestArch.rvt
            Document document = Helpers.GeneralHelper.ActiveUIDocument.Document;
            string name = document.Title;

            if (!name.Equals("TestArch.rvt"))
                Assert.Inconclusive("You must load TestArch.rvt to run these tests!");
        }

        [Test]
        public void ShouldNotHaveParameter_Test()
        {
            Document document = Helpers.GeneralHelper.ActiveUIDocument.Document;
            ParameterHelper parameterHelper = new ParameterHelper(document);
            Assert.That(parameterHelper.HasParameter("SomeParameterThatNormallyDoesnotExist"), Iz.False);
        }

        [Test]
        public void ShouldHaveParameter_Test()
        {
            Document document = Helpers.GeneralHelper.ActiveUIDocument.Document;
            ParameterHelper parameterHelper = new ParameterHelper(document);
            Assert.That(parameterHelper.HasParameter("Project Name"), Iz.True);
        }

        [Test]
        public void CanGetParameterName_Test()
        {
            Document document = Helpers.GeneralHelper.ActiveUIDocument.Document;
            ParameterHelper parameterHelper = new ParameterHelper(document);
            Assert.That(parameterHelper.HasParameter("Project Name"), Iz.True);

            Parameter para = parameterHelper.GetParameterFromProjectInfo("Project Name");
            Assert.That(para != null, "Parameter was null!");

            string name = para.GetParameterName();
            Assert.That(name.Length > 0, "An invalid string was returned trying to get the Parameter name!");
        }

        [Test]
        public void CanGetParameterInfo_Test()
        {
            Document document = Helpers.GeneralHelper.ActiveUIDocument.Document;
            ParameterHelper parameterHelper = new ParameterHelper(document);
            Assert.That(parameterHelper.HasParameter("Project Name"), Iz.True);

            Parameter para = parameterHelper.GetParameterFromProjectInfo("Project Name");
            Assert.That(para != null, "Parameter was null!");

            string name = para.GetParameterInformation(document);
            Assert.That(name.Length > 0, "An invalid string was returned trying to get the Parameter Information!");
        }
    }
}
