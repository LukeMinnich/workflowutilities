﻿using Autodesk.Revit.DB;
using Autodesk.Revit.Exceptions;
using KaterraUtilities.Extensions;
using NUnit.Framework;

namespace KaterraUtilities.Tests
{
    [TestFixture]
    public class ViewExtension_Tests
    {
        [SetUp]
        public void Setup()
        {
            // lets make sure we only run these tests on TestArch.rvt
            Document document = Helpers.GeneralHelper.ActiveUIDocument.Document;
            string name = document.Title;

            if (!name.Equals("TestArch.rvt"))
                Assert.Inconclusive("You must load TestArch.rvt to run these tests!");
        }

        [Test]
        public void Sanity_CreateViewSection_Test()
        {
            Document document = Helpers.GeneralHelper.ActiveUIDocument.Document;

            using (Transaction curTrans = new Transaction(document, "Current Transaction"))
            {
                curTrans.Start();
                Assert.Throws(typeof(ArgumentException), () => document.ActiveView.CreateViewSection(document));
                curTrans.Commit();
            }
        }

        [Test]
        public void Exception_CreateViewSection_NoTransaction_Test()
        {
            Document document = Helpers.GeneralHelper.ActiveUIDocument.Document;

            Assert.Throws(typeof(ModificationOutsideTransactionException), () => document.ActiveView.CreateViewSection(document));
        }

        [Test]
        public void Sanity_GetAllViewElements_Test()
        {
            Document document = Helpers.GeneralHelper.ActiveUIDocument.Document;

            using (Transaction curTrans = new Transaction(document, "Current Transaction"))
            {
                curTrans.Start();
                Assert.DoesNotThrow(() => document.ActiveView.GetAllViewElements(document));
                curTrans.Commit();
            }
        }
    }
}
