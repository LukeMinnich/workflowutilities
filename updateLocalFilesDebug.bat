
@echo off
if not exist C:\ProgramData\Autodesk\Revit\Addins\2017\Katerra mkdir C:\ProgramData\Autodesk\Revit\Addins\2017\Katerra
xcopy C:\_git\WorkFlowUtilities\KaterraRevitUtilities\bin\Debug C:\ProgramData\Autodesk\Revit\Addins\2017\Katerra  /y /c /s 
xcopy C:\_git\WorkFlowUtilities\KaterraUtilitiesDev.addin C:\ProgramData\Autodesk\Revit\Addins\2017 /y /c /s 
xcopy C:\_git\WorkFlowUtilities\RevitRemoteControl\bin\Debug C:\ProgramData\Autodesk\Revit\Addins\2017\Katerra\RemoteControl  /y /c /s 

xcopy C:\_git\WorkFlowUtilities\RevitRemoteControl\bin\Debug C:\_git\WorkFlowUtilities\KaterraRevitUtilities\bin\Debug\RemoteControl  /y /c /s 
