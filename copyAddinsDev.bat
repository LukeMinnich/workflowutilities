


@echo off
REM make the Katerra folder and copy in the dlls/files
if not exist C:\ProgramData\Autodesk\Revit\Addins\2017\Katerra mkdir C:\ProgramData\Autodesk\Revit\Addins\2017\Katerra
xcopy C:\_git\WorkFlowUtilities\KaterraRevitUtilities\bin\Release C:\ProgramData\Autodesk\Revit\Addins\2017\Katerra /y /c /s 
xcopy C:\_git\WorkFlowUtilities\KaterraUtilitiesDev.addin C:\ProgramData\Autodesk\Revit\Addins\2017 /y /c /s 
xcopy C:\_git\WorkFlowUtilities\RevitRemoteControl\bin\Debug C:\ProgramData\Autodesk\Revit\Addins\2017\Katerra\RemoteControl /y /c /s 


REM Only on a dev box, make the unit test folder and copy in the unit tester
if not exist C:\ProgramData\Autodesk\Revit\Addins\2017\rvtUnit mkdir C:\ProgramData\Autodesk\Revit\Addins\2017\rvtUnit
xcopy C:\_git\WorkFlowUtilities\rvtUnit\bin\Release C:\ProgramData\Autodesk\Revit\Addins\2017\rvtUnit /y /c /s 
xcopy C:\_git\WorkFlowUtilities\rvtUnit.addin C:\ProgramData\Autodesk\Revit\Addins\2017 /y /c /s 

